<?php
require_once('utility.php');
$receiveddata = file_get_contents("php://input");
require_once('./../../../aside/paymentsmanager.php');
class ConfirmMpesa extends GlobalVals
{
    var $tenid; //tenantid
    function __construct()
    {
        
      parent::__construct();
      global $receiveddata;
      $xmlobj = new SimpleXMLElement($receiveddata);
      $getallfields = $xmlobj->xpath('//c2b:C2BPaymentConfirmationRequest');
      if($getallfields)
      {
          
          foreach($getallfields as $bar)
          {
            $this->transactiontype = $bar->TransType;
            $this->transactionid = $bar->TransID;
            $this->transactiontime = $bar->TransTime ? strtotime($this->createUnixDate($bar->TransTime)) : ''; //2014-02-27-08-20-20 -- YYYYMMDDhhmmss
            $this->transactionamt = $bar->TransAmount;
            $this->businesscode = $bar->BusinessShortCode;
            $this->refno = $bar->BillRefNumber;
            $this->invnum = $bar->InvoiceNumber;
            $this->phonenum = str_ireplace('254', '', ((string)$bar->MSISDN));
            $this->orgccbal =  $bar->OrgAccountBalance;
            $this->thirdpartyid = $bar->ThirdPartyTransID;
            $this->firstname = $bar->KYCInfo->KYCName;
            $this->sname = $bar->KYCInfo->KYCValue;
            
          }
          echo $this->phonenum, "<br/>", (string)$bar->MSISDN, "<br/>";
          $mssg = 'C2B Payment Transaction '.$this->transactionid.' result received.';
          $this->returnConfirmationXml($mssg);
       
       }     
        
    }
    function confirmFields()
    {
        $tstmt = 'select id from tenant where paymentPhoneNo1 = ?';
        $this->tenid = $this->databaseSelect($tstmt, [$this->phonenum])->fetchColumn();
        return $this->transactionid && $this->transactionamt && $this->tenid;
    } 
    function validateDetails()
    {
        if($this->confirmFields())
        {
            $this->confirmInsert();
            $cative = 'select a.active, a.periodsid, b.periodName, b.year from cumulativepayments a left join paymentperiods b on a.periodsid = b.Id
             where a.tenantid = ? order by a.periodsid desc limit 1';
            $paymenthandler = new ConfirmPaymentHandler('m');
            $paymenthandler->paidamount = $this->transactionamt; 
            $paymenthandler->tenantid = $this->tenid;
            $dataarray = $this->databaseSelect($cative, [$paymenthandler->tenantid])->fetch(PDO::FETCH_ASSOC);
            //first check if there is a record in cumulative payemnts if there isn't get the first record in payment periods if no periods exist create them based on current year'
            if($dataarray)
            {
                $paymenthandler->periodid = $dataarray['active'] ? $dataarray['periodsid'] : 0;
                $stmt3 = 'select Id from paymentperiods where periodName = ? and year = ?';
                $periodname =  $dataarray['periodName'];
                $year = $dataarray['year'];
                if(!$dataarray['active'])
                {
                    //last period exists but not active
                    if($periodname+1 <= 12)
                    {
                        //check if next period is less equal 12 if not set id for next month
                        $paymenthandler->periodid = $this->databaseSelect($stmt3, [$periodname+1, $year])->fetchColumn();
                    }
                    else
                    {
                        //have to create new year and month
                        $periodname = 1;
                        $year += 1;
                        $newperiod = new PeriodGenerator();
                        $newperiod->setPeriodDefaults($year)->createNewPeriods();
                         $newid = $this->databaseSelect($stmt, [])->fetchColumn();
                        $paymenthandler->periodid = $this->databaseSelect($stmt3, [$periodname, $year])->fetchColumn();
                                              
                    }
                    
                }
            }
            else
            {
               
                $paymenthandler->periodid = $this->getPeriod();
            }
            
            
            
             //: $dataarray['periodsid'] + 1; //if last period is active set current periodsid else add 1
             if($this->businesscode)
             {
                $stmtacc = 'select accId from accounts where accName = ?';
                $tempvar2 = $this->databaseSelect($stmtacc, [$this->businesscode])->fetchColumn();
                $this->businesscode = $tempvar2 ? $tempvar2 : $this->businesscode;
                 
             }
             
            $paymenthandler->elecamt = 0;
            $paymenthandler->waterbill = 0;
            $paymenthandler->extracosts = 0;

            //
             //check if auto-payment is allowed
             
       
             $stmt2 = 'select checked from tickedsettings where field = ?';
             $autoupdate = $this->databaseSelect($stmt2, ['autoaddtosystem'])->fetchColumn();
             //first we have to insert into payments and put  on hold if succesfully run update process if autoupdate is set
             if($this->insertPayment($paymenthandler->tenantid, '1', $paymenthandler->periodid))
             {    
                                 
                 if($autoupdate)
                 {
                       if($paymenthandler->setValues()->runPaymentProcess())
                       {
                          //set payment to approved in payment
                          $this->updatePaymentStatus();
                       }
                 }
             }
             
             
            
        }
        else
        {
            $this->confirmInsert();
        }
    }
    function getPeriod()
    {
        //gets the current period to inserted for payment
        //if first period is set when tenant is being created that period will be selected when creating first payment else get first period in payment periods
        $stmt2 = 'select startPeriodId from tenant where id = ?';
        $newid = $this->databaseSelect($stmt2, [$this->tenid])->fetchColumn();  
        if(!$newid)
        {
            $stmt = 'select Id from paymentperiods order by Id asc limit 1';
            $newid2 = $this->databaseSelect($stmt, [])->fetchColumn(); 
            if(!$newid2)
            {
                $dateinfo = getdate();
                $newperiod = new PeriodGenerator();
                $newperiod->setPeriodDefaults($dateinfo['year'])->createNewPeriods();
                $newid = $this->databaseSelect($stmt, [])->fetchColumn();
                
            }
            else
            {
                $newid = $newid2;
            }
            
            
        }
        return $newid;
        
             
    }
    function confirmInsert()
    {
        //insert to paymentconfirm and logger
        $this->mcodename = 'confirm payment';
        $this->databaseInsert(false, 'confirm');
        $stmt1 = 'insert into paymentconfirm (transType, transID, transTime, transAmount, businessShortCode, billRefNumber, invoiceNumber, msisdn, OrgAccountBalance, 
        ThirdPartyTransID, firstName,lastName) values (?,?,?,?,?,?,?,?,?,?,?,?)';
        $dazzle = $this->conn->prepare($stmt1);
        $dazzle->execute([$this->transactiontype, $this->transactionid, $this->transactiontime, $this->transactionamt, $this->businesscode, $this->refno,
        $this->invnum, $this->phonenum, $this->orgccbal, $this->thirdpartyid, $this->firstname, $this->sname]);
        
        
        
    }
    function insertPayment($tid, $status, $period)
    {
        //inserts to payments
        //status whether document will be hold or approved mode
        $succesinsert = false;
        $paystmt = 'insert into payments (transId, tranDesc, accid, tenantId, phoneNo, paymentAmount, Status, paymentPeriod, 
        paymentDate, waterbill, elecbill, extracosts) values (?,?,?,?,?,?,?,?,?,?,?,?)';
        try
        {
            $this->conn->prepare($paystmt)->execute([$this->transactionid, 'm', $this->businesscode, $tid, $this->phonenum, $this->transactionamt,
        $status,$period , $this->transactiontime, 0,0,0]);
             $succesinsert =true;
        }
        catch(PDOException $e)
        {
            $succesinsert = false;
        }
        return $succesinsert;
        
    }
    function updatePaymentStatus()
    {
        $stmt = 'update payments set status = ? where transId = ?';
        return $this->conn->prepare($stmt)->execute([2, $this->transactionid]);
    }
}
class ConfirmPaymentHandler extends PaymentEngine
{
    function __construct($ttype)
    {
        parent::__construct($ttype);
    }
    
}


if($receiveddata)
{
    $validator = new ConfirmMpesa();
    $validator->validateDetails();
}


?>