<?php
require_once('utility.php');
$receiveddata = file_get_contents("php://input");
class ValidateMpesa extends GlobalVals
{
    public $localid;
    
    
    function __construct()
    {
      parent::__construct();
      global $receiveddata;
      $xmlobj = new SimpleXMLElement($receiveddata);
      $getallfields = $xmlobj->xpath('//c2b:C2BPaymentValidationRequest');
      if($getallfields)
      {
          foreach($getallfields as $bar)
          {
            $this->transactiontype = $bar->TransType;
            $this->transactionid = $bar->TransID;
            $this->transactiontime = $bar->TransTime ? strtotime($this->createUnixDate($bar->TransTime)) : ''; //2014-02-27-08-20-20 -- YYYYMMDDhhmmss
            $this->transactionamt = $bar->TransAmount;
            $this->businesscode = $bar->BusinessShortCode;
            $this->refno = $bar->BillRefNumber;
            $this->invnum = $bar->InvoiceNumber;
            $this->phonenum = (int)$bar->MSISDN;    
            $this->firstname = $bar->KYCInfo->KYCName;
            $this->sname = $bar->KYCInfo->KYCValue;
            
          }
       
       }     
        
    }
    
    function validateDetails()
    {
        //list of validation things
        //-- check if phonenumber exists 
        //-- check the option where the client allows partial payments
        $formattednum = str_ireplace('254', '', $this->phonenum);
        $stmt = 'select id, monthlyrent from tenant where paymentPhoneNo1 = ?';
        $stmt2 = 'select costPerMonth where apartment where tenantid = ?';
        $stmt3 = 'select checked from tickedsettings where field = ?';
        //check checked settings
        
        $allowpartialpayments  = $this->databaseSelect($stmt3, ['acceptpartialpayments'])->fetchColumn();
        $dtarray = $this->databaseSelect($stmt, [$formattednum])->fetch(PDO::FETCH_ASSOC);
        $rent = 0;
         if($dtarray)
         {
            //set rent value
            
            $tenantid = $dtarray['id'];
            $rent = $dtarray['monthlyrent'];
            if($rent <= 0)
            {
                $tmp = $this->databaseSelect($stmt2, [$tenantid])->fetchColumn();
                $rent = $tmp ? $tmp : 0;
                
            }
            //check partial payments
            if($allowpartialpayments)
            {
                $this->processSuccessful();
            
         
             }
            else
            {
                //doesn't allow partial payments'
                if($this->transactionamt < $rent)
                {
                    $this->mcodename = $this->returnCode('Invalid_Amount');
                    $this->returnResponseXml($this->mcodename, 'Amount is less than required', '0');
                    $this->databaseInsert(false);//call logger
                }
                else
                {
                    //insert if amount is greater equal rent 
                    $this->processSuccessful() ;
                }
             }
         }
         else
         {
            $this->mcodename = $this->returnCode('Invalid_MSISDN');
            $this->returnResponseXml($this->mcodename, 'Unregistered Number', '0');
            //call logger
            $this->databaseInsert(false);
         }
         
         //------------------------------------------
         
         
        
    }
    function processSuccessful()
    {
        //validation successful
            $this->databaseInsert(true);       
            $this->mcodename = $this->returnCode('Success');
            $this->returnResponseXml($this->mcodename, 'Payment accepted', 'Trans-'.(str_repeat('0', 6 - strlen($this->localid))).$this->localid);
            $this->databaseInsert(false);
            
    }

    
    
    
}

if($receiveddata)
{
    $validator = new ValidateMpesa();
    $validator->validateDetails();
}






?>