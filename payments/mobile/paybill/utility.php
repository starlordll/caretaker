<?php
require_once('./../../../includes/dbconnection.php');
class GlobalVals
{
    protected $transactiontype;
    protected $transactionid;
    protected $transactiontime;
    protected $transactionamt;
    protected $businesscode;
    protected $refno;
    protected $invnum;
    protected $phonenum;
    protected $firstname;
    protected $sname;
    protected $orgccbal;
    protected $thirdpartyid;  
    protected $codes;
    public $mcodename;
    protected $conn;
    function __construct()
    {
       $this->conn = DbConnector::returnconnection();
       $this->codes = array(
        'Success' => 0,
        'Invalid_MSISDN' => 'C2B00011',
        'Invalid_Account_Number' => 'C2B00012',
        'Invalid_Amount' => 'C2B00013',
        'Invalid_KYC_Details' => 'C2B00014',
        'Invalid_Shortcode' => 'C2B00015',
        'Other_Error' => 'C2B00016');  
    }
    function returnCode($codename)
    {
        return $this->codes[$codename];
    }
    function returnResponseXml($resultcode, $resultdesc, $transactionno)
    {
       $responsestr =  sprintf('
       <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:c2b="http://cps.huawei.com/cpsinterface/c2bpayment">
       <soapenv:Header/>
       <soapenv:Body>
       <c2b:C2BPaymentValidationResult>
        <ResultCode>%s</ResultCode>
        <ResultDesc>%s</ResultDesc>
	   <ThirdPartyTransID>%s</ThirdPartyTransID>
       </c2b:C2BPaymentValidationResult>
       </soapenv:Body>
       </soapenv:Envelope>', $resultcode, $resultdesc, $transactionno);
       echo($responsestr);
    }
    function returnConfirmationXml($returnmssg)
    {
       $responsetext =  sprintf('<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:c2b="http://cps.huawei.com/cpsinterface/c2bpayment">
       <soapenv:Header/>
       <soapenv:Body>
       <c2b:C2BPaymentConfirmationResult>%s</c2b:C2BPaymentConfirmationResult>
       </soapenv:Body>
       </soapenv:Envelope>', $returnmssg);
       echo($responsetext);

    }
    function databaseSelect($stmt, $params)
    {
        try
        {
            $prepared = $this->conn->prepare($stmt);
            $prepared->execute($params);
            return $prepared;
            
        }
        catch(PDOException $e)
        {
            $this->mcodename = $this->returnCode('Other_Error');
            $this->returnResponseXml($this->mcodename, 'unknown error', '0');
            exit();
        }
        
    }
    function createUnixDate($safdate)
    {
        $year  = substr($safdate, 0 , 4);
        $month = substr($safdate, 4 , 2);
        $day = substr($safdate, 6 , 2);
        $hr = substr($safdate, 8 , 2);
        $min = substr($safdate, 10 , 2);
        $sec = substr($safdate, 12 , 2);
        return "$year-$month-$day $hr:$min:$sec";
    }
    function databaseInsert($status, $requesttype = 'validate')
    {
        //status checks whether to insert into both logger and validate if false inserts only to logger else validate
            $loggerinsert = 'insert into transactionslogger (transType, transID, transTime, transAmount, businessShortCode, billRefNumber, 
            invoiceNumber, msisdn, firstName, lName, requesttype, reponsecode) values (?,?,?,?,?,?,?,?,?,?,?,?)';
            
            $loggerfields = array($this->transactiontype, $this->transactionid, $this->transactiontime, $this->transactionamt, $this->businesscode,
                $this->refno, $this->invnum, $this->phonenum, $this->firstname, $this->sname, $requesttype, $this->mcodename);
            $validateinsert = 'insert into paymentvalidate (transType, transID, transTime, transAmount, businessShortCode, billRefNumber, 
            invoiceNumber, msisdn, firstName, lastName) values (?,?,?,?,?,?,?,?,?,?)';
            
            $validatefields = array($this->transactiontype, $this->transactionid, $this->transactiontime, $this->transactionamt, $this->businesscode,
            $this->refno, $this->invnum, $this->phonenum, $this->firstname, $this->sname);
            
            
            try
            {
                $this->conn->prepare($status ? $validateinsert: $loggerinsert)->execute($status ? $validatefields: $loggerfields);
                $status ? $this->localid = $this->conn->lastInsertId() : null;
                
            }
            catch(PDOException $e)
            {
                
                $this->mcodename = $this->returnCode('Other_Error');
                $this->returnResponseXml($this->mcodename, 'unknown error', '0');
                
            }
        
        
        
    } 
}

?>