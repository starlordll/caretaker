<?php
session_start();
if(isset($_SESSION['user']) && !empty($_SESSION['user']))
{
$_SESSION['logintime'] = time();
require_once('aside/paymentsmanager.php');
require_once('includes/dbconnection.php');
require_once('includes/processimage.php');
$tenantconn = DbConnector::returnconnection();
class GetDataNFields
{
	//implemented later, sets and returns data and fields arrays, other classes that have many fields should use this
	var $dtarray;
	var $fieldsarray;
    function __construct()
    {
        $this->dtarray = array();
        $this->fieldsarray = array();
    }
    function setFieldDataArrays($getallrrays, $profname = '')
	{
		    if (isset($_REQUEST))
			{
				foreach($getallrrays as $key => $val)
				{
			       if(!empty($_REQUEST[$val]))
				   {
					   array_push($this->fieldsarray, $key);
					   if($key == 'paymentDate')
					   {
						   //convert date field before insert, should apply to all dt functions
						   array_push($this->dtarray, strtotime(str_replace('/','-',$_REQUEST[$val])));
					   }
					   else
					   {
						  array_push($this->dtarray, $_REQUEST[$val]); 
					   }
					   
				   }
				}
				if(!empty($_FILES['filename']['name']) && $profname != '')
				{
					array_push($this->fieldsarray, 'documentname');
					array_push($this->dtarray, $profname);
				}
				
		    }
	}
	function returnQMarks()
	{
		//returns the question marks for insert statement
		return join(str_split(str_repeat('?', count($this->dtarray))), ',');
	}
	function returnDtArray()
	{
		return $this->dtarray;
	}
	function returnFieldsArray()
	{
		return $this->fieldsarray;
	}
}
class Tenant
{
//mapping of the database fields 
//var $tenantId;
var $formdata;
var $firstName;
var $secondName;
//var $lastName;
var $idNumber;
var $gender;
var $isActive;
var $email;
var $boardingDate;
//var $isPaymentPeriods;
//var $paymentPeriodId;
var $paymentPhoneNo1;
var $paymentPhoneNo2;
var $nextOfKinFname;
var $nextOfKinSname;
var $nextOfKinIdNo;
var $nextOfKinPhoneId;
var $depositNumber;
var $graceperiod;
var $monthrent;
var $startperiod;
//var $currentAmount;
//var $address;
//var $pinvatno;
function convertDate($strdate){
$datearray = explode('-', $strdate);
return mktime($datearray[1],$datearray[2], $datearray[0]);
}
function __construct($formData)
{
    $this -> firstName = $formData['fname'];
    $this -> secondName = $formData['sname'];
    $this -> idNumber = $formData['idnum'];
    $this -> gender = $formData['gender'];
    $this -> isActive = $formData['tenantStatus']; 
    $this -> email = $formData['ownerEmail'];
    $this -> boardingDate =  isset($formData['boardingDate']) ? strtotime(str_replace('/','-',$formData['boardingDate'])): null;
    $this -> paymentPhoneNo1 =$formData['pnumber1'];
    $this -> paymentPhoneNo2 = ($formData['pnumber2'] != null && $formData['pnumber2'] != "") ? $formData['pnumber2'] : 0; //optional
    $this -> nextOfKinFname = $formData['kinfName'];
    $this -> nextOfKinSname = $formData['kinSName'];
    $this -> nextOfKinIdNo = $formData['kinIdNo'];
    $this -> nextOfKinPhoneId = $formData['kinPhoneNo'];
    $this -> depositAmount = ($formData['tenantDepositAmt'] != null && $formData['tenantDepositAmt'] != "") ? $formData['tenantDepositAmt']: 0 ; //optional
    $this->graceperiod = $formData['graceperiod'];
    $this->monthrent = $formData['tenantMonthlyRent'];
    $this->startperiod = $formData['periodstart']; //startPeriodId
    }
function insertTenantSql()
{
    $sqlstmt = "insert into tenant (firstName,secondName,idNumber,gender,isActive,email,boardingDate,
paymentPhoneNo1,paymentPhoneNo2,nextOfKinFname,nextOfKinSname, nextOfKinIdNo,
nextOfKinPhoneId,depositNumber, graceperiod, monthlyrent, startPeriodId) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
return $sqlstmt;
}
function getData(){
    return array($this ->firstName, $this ->secondName, $this ->idNumber, $this ->gender, $this ->isActive, $this ->email, 
    $this ->boardingDate,$this ->paymentPhoneNo1, $this ->paymentPhoneNo2, $this ->nextOfKinFname, 
    $this ->nextOfKinSname, $this ->nextOfKinIdNo, $this ->nextOfKinPhoneId, $this ->depositAmount, $this->graceperiod, $this->monthrent, $this->startperiod);
}  
function runTenant()
{
    $insertdata = new InsertData($this->insertTenantSql(), $this->getData(), '');
}  
}
class VerifyFormData
{
    var $insertedData;
    function __construct($getFormDate)
    {    
            $this->insertedData = $getFormDate;
    }
    function returnVerifiedData()
    {
        if(isset($this->insertedData) && !empty($this->insertedData))
        {
            return $this->insertedData;
        }
        else
        {
            return false;
        }                                         
    }
}
class InsertData extends PaymentEngine
{
    function __construct($stmt, $data, $profileimg, $imgpath=''){   
        parent::__construct('i');
        global $tenantconn;
        $query = $tenantconn->prepare($stmt);
        try{
        $query->execute($data);
        echo '200';
        $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : '';
        $status = isset($_REQUEST['statusselect']) ? $_REQUEST['statusselect'] :  '';
        if($page == 'payments' && $status == '2')
        {
            $this->setValues()->runPaymentProcess();
        }
        if($profileimg != '' && isset($_REQUEST['page']))
        {           
           $imgprocess = new ProcessImage($_FILES, $profileimg, $imgpath);
           $imgprocess->moveImg(); 
        }
        
         
        }
        catch(exception $e){
         echo $e -> getMessage();   
        }
        
        
  } 
}

function createInsertQuestionMarks($darray)
{
            //create question marks for sql
            $qstr = '';
            $arraycount = count($darray);
            for ($i = 0; $i < $arraycount; $i++)
            {
             if($i != ($arraycount - 1))
             {
                $qstr .= '?, ';
             }
             else
             {
                $qstr .= '?';
             }   
            }
            return $qstr;
        
}

class Apartment
{
    var $mappings = array("apartmentname"=>"aprtName", "apartmentbill"=>"costPerMonth", "apartmentdesc"=>"aprtDesc", "tenantname"=>"tenantId",  "blockname"=>"blockId");
     var $datarray = array();
     var $fieldsarray = array(); 
    function __construct($formData)
    {
        foreach($this->mappings as $key => $value)
        {
            if($formData[$key] != '' && $formData[$key] != 'None')
            {
                array_push($this->datarray, $formData[$key]);
                array_push($this->fieldsarray, $value);
            }
            //$this->datarray[$value] = ; 
        }
    }
    function runApartment() 
    {
        
        $fieldstr = implode(',', $this->fieldsarray);
        $stmt = 'insert into apartment ( '.$fieldstr.' ) values ( '.createInsertQuestionMarks($this->datarray).' )';
        new InsertData($stmt, $this->datarray, '');
 
    }
    
    
}
class Users
{
    var $username;
    var $password;
    function __construct()
    {

        
        $this->username = $_POST['usernm'];
        $this->password = $_POST['password1'];

    }
    function createStmt()
    {
        return 'insert into users (username, password) values (?, ?)';
    }
    function runUsers()
    {
        if(($this->username == '' || $this->username == 'undefined' || $this->username == ' ')  ||  ($this->password == '' || $this->password == 'undefined' || $this->password == ' '))
        {
            
            echo '400';

        }
        else
        {

            new InsertData($this->createStmt(), [trim($this->username), password_hash($this->password, PASSWORD_BCRYPT)], '');
        }
        
    }
}
class Profile
{
    var $dataarray = array();
    var $fieldsarray = array();
    var $profilephoto;
    var $profilemappings = array('fname'=>'firstName','sname'=>'secondName','lname'=>'lastName','email'=>'email','phoneno'=>'phone','postaladdr'=>'postalAddress','idno'=>'idNo','userid'=>'userID','useractive'=>'isActive');
    function __construct()
    {
         
         $this->profilephoto =  isset($_FILES['filename']['name']) && $_FILES['filename']['name'] != '' ? time().'_'.str_replace(' ', '_',$_FILES['filename']['name']) : '';
        
    }
    function setUpdateFields()
    {
        //check fields with value and add them to array
        foreach($this->profilemappings as $key => $value)
        {
            if($_REQUEST[$key] != '' && $_REQUEST[$key] != 'None')
            {
                array_push($this->dataarray, $_REQUEST[$key]);
                array_push($this->fieldsarray, $value);
            }
        }
        
    }
    function runProfileSql()
    {
        $this->setUpdateFields();
        $fieldstr = implode(',', $this->fieldsarray);
        $fieldstr .= ', profilePhoto';
        $stmt = 'insert into userdetails ( '.$fieldstr.' ) values ( '.createInsertQuestionMarks($this->dataarray).', ?)';
        array_push($this->dataarray, $this->profilephoto);
        
        new InsertData($stmt, $this->dataarray, $this->profilephoto, './images/profile/');
    }
    
}
class Estates
{
    var $estateName;
    var $estateDesc;
    var $location;
    function __construct()
    {
        if(isset($_POST['estateName']) && isset($_POST['location']))
        {
            $this->estateName = $_POST['estateName'];
            $this->location = $_POST['location'];
            $this->estateDesc = isset($_POST['estateDesc']) ? $_POST['estateDesc'] : ''; 
        }
        else
        {
            echo '400';
        }
        
        
    }
    function estatesSqlStmt()
    {
        return 'insert into estates (estateName, estateDesc, estateLocation) values ( ?, ?, ?)';
    }
    function runEstates()
    {
        new InsertData($this->estatesSqlStmt(), [$this->estateName, $this->estateDesc, $this->location], '');
    }
}
class Blocks
{
    var $blockName;
    var $blockDesc;
    var $estateId;
    function __construct()
    {
        if(isset($_POST['blockname']) && isset($_POST['estateid']))
        {
            $this->blockName = $_POST['blockname'];
            $this->blockDesc = isset($_POST['blockdesc']) ? $_POST['blockdesc'] : '' ;
            $this->estateid = $_POST['estateid']; 
        }
        else
        {
            echo '400';
        }
        
        
    }
    function blocksSqlStmt()
    {
        return 'insert into blocks (blockName, blockDesc, estateid) values ( ?, ?, ?)';
    }
    function runBlocks()
    {
        new InsertData($this->blocksSqlStmt(), [$this->blockName, $this->blockDesc, $this->estateid], '');
    }
}
class Role
{
    //customized
    function runRoles()
    {
        
        if(isset($_POST) && isset($_GET['id']))
        {
        global $tenantconn;
        $usergetter = $tenantconn->prepare('select userid from users where username = ?');
        $usergetter->execute([$_GET['id']]);
        $userid = $usergetter->fetchColumn(); 
        $countofpost = count($_POST) ; 
        $i = 0;
        $tenantconn->beginTransaction();
        $stmt = 'insert into roles (userid, screenid) values (:usrid, :screenid)'; 
        foreach($_POST as $key => $val)
        {
            $screenid = $val;
            $usrprepared = $tenantconn->prepare($stmt);
            $usrprepared->bindParam(':usrid', $userid);
            $usrprepared->bindParam(':screenid', $screenid);
            if($usrprepared->execute())
            {
                if($i == $countofpost - 1)
                {
                    echo '200';
                }
                
            }
            else 
            {
                echo '300';
            }
            $i++;
          
        }
        
        $tenantconn->commit();
    
        }

     }
    
}
class Account
{
    var $acc;
    var $accdesc;
    var $isactive;
    function __construct()
    {
      if(isset($_POST))
      {
        $this->acc =  isset($_POST['accname']) ? $_POST['accname'] : '';
        $this->accdec =  isset($_POST['accdesc']) ? $_POST['accdesc'] : '';
        $this->isactive =  $_POST['accstatus'];
      }
    }
    function runAccounts()
    {
        $stmt = 'insert into accounts (accName, accDesc, active) values (?, ?, ?)';
        new InsertData($stmt, [$this->acc, $this->accdesc, $this->isactive], '');
    }
    
    
}
class Payment extends GetDataNFields
{
	var $paymentfields;
    var $docname;
    function __construct()
    {
        parent::__construct();
       $this->paymentfields = array('transId'=> 'refid', 'tranDesc'=> 'pmethodselect' , 'accid'=> 'accselect' , 'tenantId'=> 'tenantselect', 'phoneNo'=> 'phoneno', 'paymentAmount'=> 'amount', 
	   'Status'=> 'statusselect' , 'paymentPeriod'=> 'paymentprds', 'paymentDate' => 'transdate', 'waterbill'=>'waterbill', 'elecbill'=>'elecbill', 'extracosts'=>'addcbill');
       $this->docname = isset($_FILES['filename']['name']) && $_FILES['filename']['name'] != '' ? time().'_'.str_replace(' ', '_',$_FILES['filename']['name']) : '';	   
    }   
	function insertPayments()
	{
		if(isset($_REQUEST))
		{
			$this->setFieldDataArrays($this->paymentfields, $this->docname);
			$stmt = 'insert into payments ('.join($this->returnFieldsArray(), ',').') values ('.$this->returnQMarks().')';
			new InsertData($stmt, $this->returnDtArray(), $this->docname, './images/documents/');
            echo json_encode($this->returnDtArray());
			//returnDtArray(), returnFieldsArray(), returnQMarks()
		}
	}
    
}
if(isset($_GET['page']) && !empty($_GET['page']))
{
    $verifyData = new VerifyFormData($_POST);
    if($verifyData->returnVerifiedData())
    {
         if($_GET['page'] == 'tenant')
         {
            $newtenant = new Tenant($_POST);
            $newtenant->runTenant();
         }
         if($_GET['page'] == 'apartment')
         {
            $newApartment = new Apartment($_POST);
            $newApartment->runApartment();
         }
         if($_GET['page'] == 'users')
         { 

            $newUser = new Users();
            $newUser->runUsers();

         }
         if($_GET['page'] == 'profile')
         {
            $newprofile = new Profile();
            $newprofile->runProfileSql();
         }
         if($_GET['page'] == 'estates')
         {
            
            $newestates = new Estates();
            $newestates->runEstates();
         }
         if($_GET['page'] == 'blocks')
         {
            
            $newblock = new Blocks();
            $newblock->runBlocks();
         }
         if($_GET['page'] == 'roles')
         {
            
            $newrole = new Role();
            $newrole->runRoles();
         }
         if($_GET['page'] == 'accounts')
         {
            
            $newaccount = new Account();
            $newaccount->runAccounts();
         }
		 if($_GET['page'] == 'payments')
         {
            
            $newpayment = new Payment();
            $newpayment->insertPayments();
            echo json_encode($_REQUEST);
         }
    }
    else
    {
        echo('400');
    }
}
}




?>