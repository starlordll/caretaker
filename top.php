<?php
require_once('aside/sessionsmanager.php');
$page = $_SERVER['SCRIPT_FILENAME'];$regex = '/\w*\.php$/';preg_match($regex, $page, $pagearray);
$finalpg = trim(explode('.', $pagearray[0])[0]); 
$sessionHandler = new SessionManager();
$sessionHandler->runForce();  
 

  
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">
  <!-- Bootstrap core CSS -->
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    
    
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="bootstrap/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
   

    <link href="css/sticky-footer.css" rel="stylesheet">
    <script src="js/vendor/jquery.min.js"></script>
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="bootstrap/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="bootstrap/assets/js/ie-emulation-modes-warning.js"></script>
    
<title><?php  if(isset($_SESSION['userroles'])){ echo(array_key_exists($finalpg, $_SESSION['userroles'])  ? $_SESSION['userroles'][$finalpg]['screenname'] : ucfirst($finalpg)).' - Caretaker';}else {echo ucfirst($finalpg);} ?></title>

 </head>
 <body>   
<!--<link href="css/dashboard.css" rel="stylesheet">  -->
<link href="myjs/navbarcode.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="myjs/jquery-ui.css"/>  
<!--<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">-->
<link href="myjs/fontawesome/css/font-awesome.min.css" rel="stylesheet"/>
<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php"><?php echo $_SESSION['company']; ?></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="index.php">Dashboard</a></li>
           <!-- <li><a href="notifications.php">Notifications <span class="badge">4</span></a></li> -->
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
              <?php echo ' '.$_SESSION['user'].'  '; ?><span class="caret"></span>
            </a>
               <ul class="dropdown-menu">
                    <li><a href="validate.php?page=<?php echo $finalpg ?>">Logout</a></li>             
               </ul>
            </li>
            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            About <span class="caret"></span</a>
               <ul class="dropdown-menu">
                 <li><a href="#" data-toggle="modal" data-target="#modalid">System</a></li>
                 <!--<li><a href="#">Documentation</a></li>-->
               </ul>
            
            </li>
               
          </ul>
          
          <!-- <form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Search...">
          </form> -->
        </div>
      </div>
    </nav>
<div class="container-fluid">
<div class="row no-gutter">
  <div class="col-md-2 col-sm-3 col-xs-12" style="padding-top: 50px;padding-left: 0px;margin-left: 0px;height: 100%;">
    <div class="nav-side-menu">
    <!--<div class="brand">Bomu uuu</div>-->
    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
  
        <div class="menu-list">
  
            <ul id="menu-content" class="menu-content collapse out">
               
  
          <?php 
            if(isset($_SESSION['userroles']))
            {
               $togglepgs = array('paymentperiods','users', 'company', 'notifications','notificationtemplates','settings');
               $allpagescount  = count($_SESSION['userroles']);
               $settingspgs = '';
               $i =0;
               $dateArr = getdate();
               foreach($_SESSION['userroles'] as $key => $val)
               {
  
                  $pagename = $key.'.php';
                  $pagedesc = $val['screenname'];
                  $iconclass = $val['iconclass'];
                  $modalstr  = '';
                  $dateArr = getdate();
                  $minusfunc = function($val, $val2)
                  {
                    return $val - $val2;
                  };
                  if(count($val) > 2)
                  {
                    $accessid = $key.'id';
                    $substr = '<li data-toggle="collapse" data-target="#'.$accessid.'"><a href="'.($pagename == 'configuration.php' ? '#' : $pagename).'"><i class="'.$iconclass.'"></i>'.$pagedesc.'<span class="arrow">
                    </span></a><li><ul class="sub-menu collapse" id="'.$accessid.'">';
                    $sublist = '';
                    foreach($val as $keyy => $vall)
                    {
                        if(!in_array($keyy, ['screenname', 'iconclass']))
                        {
                            eval("\$vall= \"$vall\";");
                            $sublist .= '<li><a href="'.$vall.'">'.str_replace('_',' ',$keyy).'</a></li>';
                        }
                        
                        
                    }
                    echo($substr.$sublist.'</ul>');
                  }
                  else
                  {
                    echo('<li><a href="'.$pagename.'"><i class="'.$iconclass.'"></i> '.$pagedesc.'</a></li>');
                  }
                  
                  
                }
               }
          ?>  
              
            </ul> 
</div>
</div>
<div class="modal fade" id="modalid" role="dialog">
            <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button>
                 <label>System Info</label>
               </div>
               <div class="modal-body">
               <label>Software Name</label>: Caretaker</br>
               <label>Version</label>: 1.0 </br>
               <label>Release Year</label>: 2017</br>
               <label>Vendor</label>: Lean Systems</br>
               </div>
             
            </div>
           </div>
</div>
</div>



