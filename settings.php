<?php
/*session_start();
if(!isset($_SESSION['user']))
{
  header('Location:login.php');
}*/
include_once('top.php');
?>
<script type="text/javascript">

$('#headtoggle').addClass("active"); 

</script>
<!-- content area-->
<!-- header -->
<link href="myjs/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
<link href="myjs/remodal-default-theme.css" rel="stylesheet"/>
<link href="myjs/remodal.css" rel="stylesheet"/>
<div class="col-sm-9 col-md-10 col-xs-12 main">
<div class="row top-header container-fluid">
<div class="col-sm-8">
<button class="btn btn-info glyphicon glyphicon-floppy-disk" id="save" disabled="true"> Save </button>
<button class="btn btn-info glyphicon glyphicon-edit" id="edit"> Edit </button>
<div id="requiredError" data-role="popup" ></div>     
</div>
<div class="col-sm-4">

</div>
</div>
<hr />
<!-- content area-->
<!-- header -->
<!-- endheader -->
<div class="row">
    <div class="col-md-4">
        <h3><center>Payments Settings</center></h3>
        <hr />
        <div class="row">
        <div class="form form-group">
            <div class="col-md-4"><input type="checkbox" id="referenceid" class="form-control"/></div>
            <div class="col-md-8"><b>Use Auto-Numbering for Reference ID </b> </div>
        </div>      
        </div>
        <br />
        <div class="row">
        <div class="form form-group">
            <div class="col-md-4"><input type="checkbox" id="autoaddtosystem" class="form-control"/></div>
            <div class="col-md-8"><b data-toggle="tooltip" title="will automatically set the mpesa payments to approved and add their amounts into the system otherwise 
            will leave them on hold">Automatically Approve Mpesa Payments </b> </div>
        </div>      
        </div>
        <br />
         <div class="row">
        <div class="form form-group">
            <div class="col-md-4"><input type="checkbox" id="acceptpartialpayments" class="form-control"/></div>
            <div class="col-md-8"><b data-toggle="tooltip" title="if set to unchecked, will reject any mpesa sent amount that is less than rent amount">Accept Partial Mpesa Payments </b> </div>
        </div>
    </div>
    

</div>


    <div class="col-md-4">
        <h3><center>Period Settings</center></h3>
        <hr />
        <div class="row">
        <div class="col-md-3">
           </div>
           <div class="col-md-6">
              <div class="form form-group">
                 <label>Year Start: </label>
                 <div class="input input-group">
                 <input type="text" class="form-control" id="startdate" value="<?php echo(getdate()['year']); ?>"/>
                 <span class="input input-group-addon glyphicon glyphicon-calendar"></span>
                 </div>
              </div>
               
           </div>
           <div class="col-md-3">
           </div>
            
        </div>
        <div class="row">
           <div class="col-md-3">
           </div>
           <div class="col-md-6">
               <div class="form form-group">
                  <button class="btn btn-info" id="genperiods"> Generate New Periods</button>
               </div>
           </div>
           <div class="col-md-3">
           </div>
            
        </div>
        
        
    <br />
    </div>
    <div class="col-md-4">
       <!--  <h3><center>Initial Settings</center></h3> 
        <hr />-->
    </div>

</div>
<script type="text/javascript" src="myjs/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="pagesjs/settings.js"></script>
<script type="text/javascript" src="myjs/notify.min.js"></script>

<!-- end of content area-->
<?php


include_once('bottom.php');
?>