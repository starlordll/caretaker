<?php
require_once('aside/sessionsmanager.php');
$page = $_SERVER['SCRIPT_FILENAME'];$regex = '/\w*\.php$/';preg_match($regex, $page, $pagearray);
$finalpg = trim(explode('.', $pagearray[0])[0]); 
$sessionHandler = new SessionManager();
$sessionHandler->runForce();  
 

  
?>
<html>
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="bootstrap/favicon.ico">
  <!-- Bootstrap core CSS -->
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    
    
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="bootstrap/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
   

    <link href="css/sticky-footer.css" rel="stylesheet">
    <script src="js/vendor/jquery.min.js"></script>
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="bootstrap/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="bootstrap/assets/js/ie-emulation-modes-warning.js"></script>
<title><?php echo $_SESSION['userroles'][$finalpg]['screenname'].' - Caretaker'; ?></title>

<style>
h4 .iconheaders{
    text-align: center;
}
.gutter > [class*='col-']{
    padding-right: 0px;
    padding-left: 10px;
}
.no-gutter > [class*='col-']{
    padding-right: 0;
    padding-left: 0;
}
.nav-side-menu {
  overflow: auto;
  font-family: verdana;
  font-size: 12px;
  font-weight: 200;
  background-color: #2e353d;
  position: relative;
  top: 0px;
  padding-top: 52px;
  width: auto;
  max-width: 300px;
  min-width: 250px;
  height: 100%;
  color: #e1ffff;
}
.nav-side-menu .brand {
  background-color: #23282e;
  line-height: 50px;
  display: block;
  text-align: center;
  font-size: 14px;
}
.nav-side-menu .toggle-btn {
  display: none;
}
.nav-side-menu ul,
.nav-side-menu li {
  list-style: none;
  padding: 0px;
  margin: 0px;
  line-height: 35px;
  cursor: pointer;
  /*    
    .collapsed{
       .arrow:before{
                 font-family: FontAwesome;
                 content: "\f053";
                 display: inline-block;
                 padding-left:10px;
                 padding-right: 10px;
                 vertical-align: middle;
                 float:right;
            }
     }
*/
}
.nav-side-menu ul :not(collapsed) .arrow:before,
.nav-side-menu li :not(collapsed) .arrow:before {
  font-family: FontAwesome;
  content: "\f078";
  display: inline-block;
  padding-left: 10px;
  padding-right: 10px;
  vertical-align: middle;
  float: right;
}
.nav-side-menu ul .active,
.nav-side-menu li .active {
  border-left: 3px solid #d19b3d;
  background-color: #4f5b69;
}
.nav-side-menu ul .sub-menu li.active,
.nav-side-menu li .sub-menu li.active {
  color: #d19b3d;
}
.nav-side-menu ul .sub-menu li.active a,
.nav-side-menu li .sub-menu li.active a {
  color: #d19b3d;
}
.nav-side-menu ul .sub-menu li,
.nav-side-menu li .sub-menu li {
  background-color: #181c20;
  border: none;
  line-height: 28px;
  border-bottom: 1px solid #23282e;
  margin-left: 0px;
}
.nav-side-menu ul .sub-menu li:hover,
.nav-side-menu li .sub-menu li:hover {
  background-color: #020203;
}
.nav-side-menu ul .sub-menu li:before,
.nav-side-menu li .sub-menu li:before {
  font-family: FontAwesome;
  content: "\f105";
  display: inline-block;
  padding-left: 10px;
  padding-right: 10px;
  vertical-align: middle;
}
.nav-side-menu li {
  padding-left: 0px;
  border-left: 3px solid #2e353d;
  border-bottom: 1px solid #23282e;
}
.nav-side-menu li a {
  text-decoration: none;
  color: #e1ffff;
}
.nav-side-menu li a i {
  padding-left: 10px;
  width: 20px;
  padding-right: 20px;
}
.nav-side-menu li:hover {
  border-left: 3px solid #d19b3d;
  background-color: #4f5b69;
  -webkit-transition: all 1s ease;
  -moz-transition: all 1s ease;
  -o-transition: all 1s ease;
  -ms-transition: all 1s ease;
  transition: all 1s ease;
}
@media (max-width: 767px) {
  .nav-side-menu {
    position: relative;
    width: 100%;
    margin-bottom: 10px;
  }
  .nav-side-menu .toggle-btn {
    display: block;
    cursor: pointer;
    position: absolute;
    right: 10px;
    top: 10px;
    z-index: 10 !important;
    padding: 3px;
    background-color: #ffffff;
    color: #000;
    width: 40px;
    text-align: center;
  }
  .brand {
    text-align: left !important;
    font-size: 22px;
    padding-left: 20px;
    line-height: 50px !important;
  }
}
@media (min-width: 767px) {
  .nav-side-menu .menu-list .menu-content {
    display: block;
  }
}
body {
  margin: 0px;
  padding: 0px;
}

    </style>
 </head>
 <body>   
 <link href="css/dashboard.css" rel="stylesheet">  
<link href="css/main.css" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="myjs/jquery-ui.css"/>  
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">Caretaker</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="index.php">Dashboard</a></li>
            <li><a href="notifications.php">Notifications <span class="badge">4</span></a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
              <?php echo ' '.$_SESSION['user'].'  '; ?><span class="caret"></span>
            </a>
               <ul class="dropdown-menu">
                    <li><a href="validate.php?page=<?php echo $finalpg ?>">Logout</a></li>             
               </ul>
            </li>
            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            About <span class="caret"></span</a>
               <ul class="dropdown-menu">
                 <li><a href="#">System</a></li>
                 <li><a href="#">Documentation</a></li>
               </ul>
            
            </li>
               
          </ul>
          <!-- <form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Search...">
          </form> -->
        </div>
      </div>
    </nav>
<div class="container-fluid">
<div class="row no-gutter">
    <div class="col-md-3 col-sm-2 nav-side-menu">
    <!--<div class="brand">Bomu uuu</div>-->
    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
  
        <div class="menu-list">
  
            <ul id="menu-content" class="menu-content collapse out">
               
  
          <?php 
            if(isset($_SESSION['userroles']))
            {
               $togglepgs = array('paymentperiods','users', 'company', 'notifications','notificationtemplates','settings');
               $allpagescount  = count($_SESSION['userroles']);
               $settingspgs = '';
               $i =0;
               foreach($_SESSION['userroles'] as $key => $val)
               {
  
                  $pagename = $key.'.php';
                  $pagedesc = $val['screenname'];
                  $iconclass = $val['iconclass'];
                  $modalstr  = '';
                  if(count($val) > 2)
                  {
                    $accessid = $key.'id';
                    $substr = '<li data-toggle="collapse" data-target="#'.$accessid.'"><a href="'.$pagename.'"><i class="'.$iconclass.'"></i>'.$pagedesc.'<span class="arrow">
                    </span></a><li><ul class="sub-menu collapse" id="'.$accessid.'">';
                    $sublist = '';
                    foreach($val as $keyy => $vall)
                    {
                        if(!in_array($keyy, ['screenname', 'iconclass']))
                        {
                            $sublist .= '<li><a href="'.$vall.'">'.$keyy.'</a></li>';
                        }
                        
                        
                    }
                    echo($substr.$sublist.'</ul>');
                  }
                  else
                  {
                    echo('<li><a href="'.$pagename.'"><i class="'.$iconclass.'"></i> '.$pagedesc.'</a></li>');
                  }
                  
                  
                }
               }
          ?>  
              
            </ul> 
</div>
</div>

    
    
    
    
    


<!-- 
//$t = Dbconnector::returnconnection()->query('select name, additionalinfo from screens where parentId  = 16')->fetchAll(PDO::FETCH_ASSOC);
//session_start();
//var_dump($_SESSION['userroles']);



//$f = array('hj' => array('screenname' => 'name', });


/*
require_once($_SERVER['DOCUMENT_ROOT'].'/reportico/reportico.php');
$a = new reportico();
$a->embedded_report = true;
$a->initial_execute_mode = "EXECUTE";
$a->initial_project = "caretaker";
$a->initial_report = 'overdue.xml';
$a->clear_session = 1;
$a->initial_output_format = 'HTML';
$a->initial_execution_parameters = array();
$a->initial_execution_parameters['periodnm'] = '4';
$a->initial_execution_parameters['tenantid'] = '1';
$a->access_mode = 'REPORTOUTPUT';
$a->execute();


*/
















/*
include_once('includes/dbconnection.php');
$r = file('employess.csv');
$fname = 'Letty';
$lname = 'Dopee';
$stmt = 'insert into tenant (firstName, secondName, idNumber, gender, email, isActive, boardingDate,  paymentPhoneNo1, 
nextOfKinFname, nextOfKinSname, nextOfKinIdNo, 
nextOfKinPhoneId, depositNumber, graceperiod, monthlyrent) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
$connector = Dbconnector::returnconnection();

$connector->beginTransaction();

try
{
    $hj=$connector->prepare($stmt);
    foreach($r as $dope)
    {
      $g = explode(',', $dope);
      //echo count((int)trim($g[0])+((int)trim($g[4]))), "<br/>", $g[4] + $g[0],"<br/>", $g[0] ,"<br/>";
      $hj->execute([trim($g[1]), trim($g[2]), trim($g[0])*200, 1, trim($g[3]), 1, 1488315600000,(int)($g[0]+$g[4]), $fname, $lname, trim($g[0])*300, (int)trim($g[4]),500, 3, 13000]);
      $fname =$g[1];
      $lname = $g[2];
    }
     $connector->commit();
     echo 'done';
}
catch(PDOException $e)
{
    echo $e->getMessage();
    $connector->rollBack();
}
*/