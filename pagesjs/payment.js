$(document).ready(function(){
    
    var paymentdataset = [];
    var paymentid = '';
    var docpath = './images/documents/';
    var docname = '';
    var docstatus;
    
    var paymentfields = { 'refid' : 'required', 'paymentprds' : 'notrequired', 'tenantselect' : 'required', 'statusselect' : 'notrequired',
    'pmethodselect' : 'required' , 'accselect' : 'required', 'phoneno' : 'required', 'amount' : 'required', 'transdate' : 'required',
     'elecbill' : 'notrequired', 'waterbill' : 'notrequired', 'addcbill' : 'notrequired'
        
    }; 
    //tenantselect,pmethodselect,accselect
    //'amount,elecbill, waterbill, addcbill'
  $(function () { $('#dt').datetimepicker({format: 'dd/mm/yyyy', startView : 2, minView : 2, autoclose : true}); }); 
   
   populatePeriods('paymentprds');
   populateTenant();
   populateAccounts();
   setReferenceID();
   autocompleter('searchpayment', 'autocomplete.php?page=payments', paymentsGetID);
   function paymentsGetID(event, ui)
   {
      var qryterm = typeof ui == 'string' ? ui : ui.item.value; 
      $.ajax({
            url: 'sendBackStuff.php?qfield=none&page=payment&id='+qryterm,
            dataType : 'json',
            success: function(data){ 

                setPaymentValues(data, qryterm);             
             
            } 
            
    });
   }
   function setPaymentValues(dtarray, pgid)
   {
      $('#imgmessage').html('');
      $('#attachmessage').html('Attach scanned copy');
      $('#displayimg').attr('src', '');
      docname = '';
      paymentdataset = dtarray; 
      
      paymentid = dtarray[0];
      docstatus = 1;
      if(paymentdataset.length > 0)
      {
       $('#refid, #tenantselect, #pmethodselect, #accselect, #phoneno, #amount, #transdate').trigger('click');
       toggleBtns(false);
       $("#receipt").prop('disabled', false);
       var tmpdocname = paymentdataset[paymentdataset.length - 1];
       if(tmpdocname != '' && tmpdocname != null)
       {
          docname = tmpdocname;
          $('#imgmessage').html('');
          $('#attachmessage').html('Attached (1)');
          $('#displayimg').attr('src', docpath+docname);
          
       }
        
        
        var i =0;
        $.each(paymentfields, function(key, val){
            
            paymentdataset[i] != null   ?  $('#'+key).val(paymentdataset[i]) : paymentdataset[i] = '' ;
            key == 'statusselect' ? docstatus = paymentdataset[i] : null;
            i++;
            
        });
      
       //toggle field status with regards to document status
       if(docstatus == 1)
       {
          setFieldStatus(paymentfields, false);
          $('#tenantselect').trigger('change');
          $('#viewbtn, #filecopy, #save').prop('disabled', false);
          toggleBtns(false)
       }
       else
       {
          setFieldStatus(paymentfields, true);
          $('#viewbtn, #filecopy, #save').prop('disabled', true);
          toggleBtns(true)
       }
       

      }
    
   }
   $('#accbtn').click(function(event){
      var url = $('#accselect').val() != 'none' ? 'accounts.php?id='+$('#accselect').val() :  'accounts.php';
      openWindow(url);
   });
   //populate accounts
   function populateAccounts()
   {
       $.getJSON('dropdowns.php', { 'page' : 'payment' ,'dropdownid' : 'accounts' }, function(dt){
       
        var options = '';
         $.each(dt, function(key, val){
            options += '<option value="'+val[0]+'">'+val[1]+'</option>'
         });
         $('#accselect').append(options);
       });
   }
   function populateTenant()
   {
       $.getJSON('dropdowns.php', { 'page' : 'payment' ,'dropdownid' : 'tenant' }, function(dt){
        var options = '';
         $.each(dt, function(key, val){
            options += '<option value="'+val[0]+'">'+val[1]+' '+val[2]+'</option>'
         });
         $('#tenantselect').append(options);
       });
     
   }
   //set values on the periods page

   function setReferenceID()
   {
     $.getJSON('sendBackStuff.php', {'page' : 'payment','id' : 'none', 'qfield' : 'referenceid'}, function(dta){
          if(dta[0] != 'false')
          {
          
            $('#refid').val(dta[0]);
            
          }
          else
          {
            $('#refid').val('');
          }
     });
   }
   //events
   //save
   $('#save').click(function(event)
   {
       saveUpdate();
   });
  
   function saveUpdate()
   {
    if(!genValidateFields(paymentfields))
    {
        var paymentinsertobj = new FormData();
        var updatable = false;
        if($('#filecopy').val() != '')
        {
               paymentinsertobj.append('filename', $('#filecopy')[0].files[0]);
               updatable = true;
        }
        if(paymentdataset.length > 0 && paymentid != '')
         { 
           //update
           var i = 0;
           $.each(paymentfields, function(key, val){
            if($('#'+key).val() != paymentdataset[i])
            {
                paymentinsertobj.append(key, $('#'+key).val());
                updatable = true;
            }
            i++;
           });
           
           
           if(updatable)
           {
               paymentAjax(paymentinsertobj, 'updateStuff.php?page=payments&id='+paymentid, messageAfterUpdate);
           }       
         }
         else
         {
           //insert
           
           $.each(paymentfields, function(key, value){
              
              paymentinsertobj.append(key, $('#'+key).val());
            
           });
           paymentAjax(paymentinsertobj, 'insertStuff.php?page=payments', messageAfterInsert);
           
         }
    }
    
   }
   function messageAfterUpdate(dt)
   {
    
    if(dt == 200)
    {
        defineErrorCodes(dt, 'Update', '');
        $('#new').trigger('click');
    }
    else
    {
        defineErrorCodes(dt, 'Update', '');
    }
     
      //msgNotifier ( )
      
   }
   function messageAfterInsert(dt)
   {
   
    if(dt == 200)
    {
        defineErrorCodes(dt, 'Insert', '');
        $('#new').trigger('click');
    }
    else
    {
        defineErrorCodes(dt, 'Insert', '');
    }
    
    
   }
   function paymentAjax(senddata, url, callbackfunc)
   {
         $.ajax(
           {
            url : url,
            type : 'POST',
            processData : false,
            contentType : false,
            cache : false,
            data : senddata,
            success : function(data){ 
                callbackfunc(data);
            },
                
            error : function(errr){ callbackfunc(errr);  }
           });       
   }
   //cancel
   $('#cancel').click(function(event){
    if(!genValidateFields(paymentfields))
    {
      var confirmmodal = $('[data-remodal-id=modal]').remodal();
      modaldetails('Cancellation Confirmation', 'Do you wish to cancel this payment? kindly note that once cancelled it will be set to read-only and cannot be reverted.', 'Confirm');
      confirmmodal.open();
      $(document).on('confirmation', '.remodal', function () {
           $('#statusselect').val('3');
           setFieldStatus(paymentfields, true);
           $('#viewbtn, #filecopy, #save').prop('disabled', true);
           toggleBtns(true);
           saveUpdate();
       });
     }
     /* $(document).on('cancellation', '.remodal', function () {
           console.log('Cancel button is clicked');
       });*/
   });
   //approve
   $('#approve').click(function(event){
    
      if(!genValidateFields(paymentfields))
      {
        var confirmmodal = $('[data-remodal-id=modal]').remodal();
        modaldetails('Approve Document', 'Kindly note that once approved the document will be set as Read Only and cannot be changed. ', 'Approve');
        confirmmodal.open();
        $(document).on('confirmation', '.remodal', function () {
           $('#statusselect').val('2');
           setFieldStatus(paymentfields, true);
           $('#viewbtn, #filecopy, #save').prop('disabled', true);
           toggleBtns(true);
           saveUpdate();
        });
        
      }
      
   
   });
   $("#search").click(function(event){
        paymentsGetID(event, $('#searchpayment').val());
   });
   function modaldetails(header, messagedet, buttonst)
   {
       $('h1#modalheader').html(header);
       $('p#modalinfo').html(messagedet);
       $('#modalconfirm').html(buttonst);
   }
   //new
   $('#new').click(function(event){
    $("#receipt").prop('disabled', true);
      $('#refid, #tenantselect, #pmethodselect, #accselect, #phoneno, #amount, #transdate').trigger('click'); //remove fields with red
      $.each(paymentfields, function(key, val){
        if($.inArray(key, ['paymentprds','tenantselect','statusselect','pmethodselect', 'accselect']) != -1)
        {
            $.inArray(key, ['statusselect','paymentprds']) == -1 ? $('#'+key).val('None') : null;
        }
        else
        {
            $.inArray(key, ['amount','elecbill', 'waterbill', 'addcbill']) != -1 ? $('#'+key).val('0.00') : $('#'+key).val('');;
            
        }
        
      });
      setFieldStatus(paymentfields, false);
      $('#viewbtn, #filecopy, #save').prop('disabled', false);
       $('#imgmessage').html('');
       $('#statusselect').val('1')
      $('#attachmessage').html('Attach scanned copy');
      $('#displayimg').attr('src', '');
      $('#amtdue').val('0.00')
      toggleBtns(true);
      paymentdataset = [];
      paymentid = '';
      docname = '';
      setReferenceID();
      
   });
   function clearEverything()
   {
      $('#new').trigger('click');
   }
   $('#refid, #tenantselect, #pmethodselect, #accselect, #phoneno, #amount, #transdate').click(
   function(event){
        if($('#'+event.target.id).hasClass('alert alert-danger'))
       {
          $('#'+event.target.id).removeClass('alert alert-danger');
         $('#requiredError').html('');
        }
   });
   
   function toggleBtns(status)
   {
      $('#approve, #cancel').prop('disabled', status);
      //$('').prop('disabled', status);
      //$('').prop('disabled', status);
   }
   $("#receipt").click(function(event){
    var refid = paymentid != '' ? '&refid='+paymentid : '' ;
    window.location = 'run.php?project=caretaker&xmlin=receipt.xml&execute_mode=EXECUTE&clear_session=1&target_show_criteria=0'+refid;
   })
   //validate uploaded img/doc
    $('#filecopy').bind('change', function(event){
           var formData = new FormData();
           formData.append('filename', $('#filecopy')[0].files[0]);
           paymentAjax(formData, 'validateimages.php', imgnotifications);
	       
    });
    //set the minimum period for each tenant
    $('#tenantselect').on('change', function(event){
        var tenantid = $('#tenantselect').val();
        if(tenantid != '')
        {
            var dts = {'page': 'payment',"dropdownid": 'paymenttenant', 'tenantid' : tenantid };
            $.ajax(
            {
                url : 'dropdowns.php',
                dataType : 'json',
                type : 'POST',
                data : dts,
                success : function(dtm)
                {
                    $('.periodsselect').remove();
                    appendToPeriods(dtm, 'paymentprds');
                    $('#amtdue').val(dtm[dtm.length - 1][0]);
                    $('#phoneno').val(dtm[dtm.length - 1][1]);
                    
                },
                error : function(err)
                {
                    console.log(err);
                    
                }
                
            });
        }
        
    });
    function imgnotifications(data)
    {
               if(data != '200')
                {
                    $('#imgmessage').html(data);
                     $('#filecopy').val('');
                     $('#attachmessage').html('Attach scanned copy');
                } 
                else
                {
                    $('#imgmessage').html('');
                    $('#attachmessage').html('Attached (1)');
                }
                
    }
    var prevnextcounter = 0;
    $('#Next, #Previous').click(function(event){
        if(prevnextcounter == 0 || docstatus == 1){  $('.periodsselect').remove(); populatePeriods('paymentprds');};
        PrevNextBtns(event.currentTarget.id);
        prevnextcounter +=1;

    });
    function PrevNextBtns(Statuses)
    {
        var currID = paymentid ? paymentid : 'NoID';
        $.ajax({
            url: 'sendBackStuff.php?page=payment&qfield=none&statusPN='+Statuses+'&id='+currID,
            dataType : 'json',
            success: function(data){
                setPaymentValues(data, '');
                if(data == '300')
                {
                    $.notify('couldnt get more data', 'Warning');
                    clearEverything();
                    
                }
                if(data == '400')
                {
                    $.notify('error while getting data', 'Error');
                    clearEverything();
                }
             
            } 
            
            });
}
    insertFromQuery('payment&qfield=none', setPaymentValues);
   
});