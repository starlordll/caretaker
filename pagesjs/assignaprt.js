$(document).ready(function(){
   $('#modalstart').click(function(event){
            sendReceive('dropdowns.php', {'page' : 'apartments', 'dropdownid' : 'tenant'}, populateTenant);
            sendReceive('dropdowns.php', {'page' : 'apartments', 'dropdownid' : 'apartment'}, populateApartment);
            function populateTenant(dtp)
            {
                var ops= '';
                $.each(dtp, function(key, value){
                    ops += '<option class="tenantnames" value="'+$.trim(value[0])+'">'+value[1]+' '+value[2]+'</option>';
                });
                $(".tenantnames").remove();
                $('#tenctrl').append(ops);
            }
            function populateApartment(dta)
            {
                var ops= '';
                $.each(dta, function(key, value){
                    ops += '<option class="apartmentnms" value="'+$.trim(value[0])+'">'+value[0]+' '+value[1]+' - '+value[2]+'</option>';
                });
                $(".apartmentnms").remove();
                $('#aprtctrl').append(ops);
            }
            
            
            
            
            $('#idmodal').modal();
            
            
            
          });
    function sendReceive(dturl, senddata, callbackfunc)
    {
                $.ajax(
                {
                    url : dturl,
                    data : senddata,
                    dataType : "JSON",
                    type : "POST",
                    success : function(dt)
                    {
                        callbackfunc(dt);
                    },
                    error : function(err)
                    {
                        callbackfunc(err);
                    }
                });
    } 
   $('#aprtbtn').click(function(event){
    //console.log('clicked assign');
       if($('#aprtctrl').val() == 'None')
       {
          $('#errormsges').html('Apartment must be selected !!').addClass('alert alert-danger');
          
       }
       else
       {
         sendReceive('updateStuff.php?page=apartment&id='+$('#aprtctrl').val(), {'tenantname' : $('#tenctrl').val() == 'None' ? null : $('#tenctrl').val()}, updateCallback);
       }
   });
   function updateCallback(dt)
   {
    console.log(dt);
    if(dt == '200')
    {
        $('#errormsges').html('Apartment assigned successfully').addClass('alert alert-success');
    }
    else
    {
        $('#errormsges').html('Error occurred while updating tenant').addClass('alert alert-danger');
    }
   }
    
    
    
    
});