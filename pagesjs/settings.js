$(document).ready(function(){
$(function () { $('#startdate').datetimepicker({format: 'yyyy', minView : 2}); });


settingsfields = {referenceid : {id : '', checkstatus : ''}, autoaddtosystem : {id : '', checkstatus : ''}, acceptpartialpayments : {id : '', checkstatus : ''}};
senderGetter('aside/settingsops.php', {'action' : 'g'}, setDefaults);
senderGetter('aside/settingsops.php', {'action' : 'pr'}, setYear); 
 
 
 function setDefaults(data)
 {
    $.each(data, function(key, val){
        $('#'+val.field).prop('checked', val.checked);
        settingsfields[val.field].id = val.id;
        settingsfields[val.field].checkstatus = val.checked;
        
    });
    
 }
 $('#save').click(function(event){
    var updateobj = {};
    var refid = $('#referenceid').is(':checked') ? 1 : 0;
    var autoadd = $('#autoaddtosystem').is(':checked') ? 1 : 0;
    var partialpayment = $('#acceptpartialpayments').is(':checked') ? 1 : 0;
    if(settingsfields.referenceid.id && ( refid != settingsfields.referenceid.checkstatus))
    {
        updateobj['id'] = settingsfields.referenceid.id;
        updateobj['checked'] = refid;
    }
    if(settingsfields.autoaddtosystem.id && ( autoadd != settingsfields.autoaddtosystem.checkstatus))
    {
        updateobj['id'] = settingsfields.autoaddtosystem.id;
        updateobj['checked'] = autoadd;
    }
    if(settingsfields.acceptpartialpayments.id && ( partialpayment != settingsfields.acceptpartialpayments.checkstatus))
    {
        updateobj['id'] = settingsfields.acceptpartialpayments.id;
        updateobj['checked'] = partialpayment;
    }
    if(Object.keys(updateobj).length > 0)
    {
        
        updateobj['action'] = 'u';
        senderGetter('aside/settingsops.php',updateobj, updatecallback);
        senderGetter('aside/settingsops.php', {'action' : 'g'}, setDefaults);
    }
    $('#save').prop('disabled', true);
 });
 $('#edit').click(function(event){
    $('#save').prop('disabled', false);
 });
 $('#genperiods').click(function(event){
    senderGetter('aside/settingsops.php', {'action' : 'pg', 'year' : $('#startdate').val()}, yeargenerate);
 });
 function yeargenerate(dt)
 {
    if(dt == '200')
    {
        $.notify('New Period Created', 'success');
    }
    else if(dt == '300')
    {
        $.notify('No period was created', 'info');
    }
    else
    {
        $.notify('An error occurred', 'error');
    }
 }
 function senderGetter(urlname, dt, callfunc)
 {
     $.ajax(
     {
        url : urlname,
        type : 'POST',
        dataType : 'json',
        data : dt,
        success : function(data)
        {
            //console.log(data);
            callfunc(data);
        },
        error : function(errs)
        {
            //console.log(data);
            $.notify('an error occurred', 'error');
        }
 });
 }
 function setYear(st)
 {
    if(st.length > 0)
    {
        $('#startdate').val(st[0]);
        $('#startdate').prop('disabled', true);
    }
 }
 function updatecallback(dt)
 {
    if(dt == '200')
    {
        $.notify('Settings saved', 'success');
    }
    else if(dt == '300')
    {
        $.notify('No settings were updated', 'info');
    }
    else
    {
        $.notify('An error occurred', 'error');
    }
 }

 
    
});