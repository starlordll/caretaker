$(document).ready(function() {
  
var tenantdataset = [];
var tenantID = "";
var everyField = {'fname' : 'required','sname' : 'required','idnum' : 'required', 'gender' : 'required', 'tenantStatus' : 'notrequired', 'ownerEmail' : 'required', 'boardingDate' : 'required', 'pnumber1' : 'required', 'pnumber2' : 'notrequired',
'kinfName' : 'required','kinSName' : 'required','kinIdNo' : 'required','kinPhoneNo' : 'required','tenantDepositAmt' : 'required', 'graceperiod' : 'notrequired', 'tenantMonthlyRent' : 'required', 'periodstart' : 'required'};
 //store all the required class fields which are empty 
 //store all class fields that have values
//code for search//in edit mode stores all the fields that have been updated
 //stores the current data from database


// event regions
populatePeriods('periodstart');
autocompleter("searchTenant",'autocomplete.php?page=tenant', callSetValues);
function callSetValues(event, ui)
{
    $.ajax({
            url: 'sendBackStuff.php?page=tenant&id='+getID(ui.item.value),
            dataType : 'json',
            success: function(data){

                setValueInFields(data, ui.item.value);
                
             
            } 
            
    });
}
$(function () { $('#dater').datetimepicker({format: 'dd/mm/yyyy', startView : 2, minView : 2, autoclose : true}); }); 
$("#save").click(function(event) {
	/* Act on the save event */
    if(!genValidateFields(everyField))
    
    { 
            if(tenantdataset && tenantID != '')
             {
              //update
              var toUpdateObj = {};
              var i = 1;
              var tenStatus = $('#tenantStatus').prop('checked') ? 1 : 0;

              $.each(everyField, function(key, val){
                    
                    if($.inArray(key, ['tenantStatus']) != -1)
                    {
                        
                    
                    if(key == 'tenantStatus' && tenantdataset[i] != tenStatus)
                    {
                       toUpdateObj[key] =  tenStatus;
                    }
                   
                    }
                    else
                    {
                        if(($('#'+key).val()).trim() != (String(tenantdataset[i])).trim())
                        {
                            toUpdateObj[key] = $('#'+key).val();
                        }
                    }
                    
                 i++; 
              });
              if(Object.keys(toUpdateObj).length > 0)
              { 
                //console.log(tenantdataset);
                //console.log(tenantdataset[tenantdataset.length - 1]);
                ajaxSendReceive('updateStuff.php?page=tenant&id='+tenantID, toUpdateObj, 'Update', '');
              }
                  
              
        
             }
            else
            {
                  //insert
                  var insertingObj = {}
                  $.each(everyField, function(key, val){
                    
                    if(key == 'tenantStatus')
                    {
                        insertingObj[key] = $('#tenantStatus').is(':checked') ? 1 : 0;
                    }
                    else
                    {
                        insertingObj[key] = $('#'+key).val();
                    }
                    
                  });
                  ajaxSendReceive('insertStuff.php?page=tenant', insertingObj, 'Insert', '');
            }
    }

});
$('#getPrevious').click(
function(event)
{
    var getStatus = "Prev";
    prevNextBtn(getStatus);
    
});
$('#getNext').click( function(event)
{
    var getStatus = "Next";
    prevNextBtn(getStatus);
    
});
//function to clear
function clearFields()
{
    $.each(everyField, function(tenanttag, val){
       if($.inArray(tenanttag, ['gender', 'periodstart']) != -1)
       {
        $('#'+tenanttag).val('None');
        
       }
       else if(tenanttag == 'tenantStatus')
       {
          $('#tenantStatus').prop('checked', false);
       }
       else
       {
          $('#'+tenanttag).val('');
       }
       
       
        
    });
     
}

$('#new').click(function(event)
{
    
    tenantAfterDelete();
    $("#requiredError").html("");
    $("#save").prop('disabled', false);
});
$("#edit").click(
function(event){
    $("#save, #new").prop('disabled', false);
    setFieldStatus(everyField, false);
    $('#dater').prop('disabled', false);
    //loadButtonStatuses(true);
    $("#searchTenant").val("");
    
});
$('#delete').click(function(event){
    if(tenantID != '')
    {
       deleteRecord("deleteStuff.php?page=tenant", tenantID, tenantAfterDelete);    
    }
});
function tenantAfterDelete()
{
    clearFields();
    $("#searchTenant").val("");
    tenantID = "";
    tenantdataset = [];
    setFieldStatus(everyField, false);
    $('#dater').prop('disabled', false);
}

function getID(idWithName)
{
    //returns the id from the name on the search bar
    return idWithName.split(".")[0];
}

//set values in fields
function setValueInFields(parsedArray, id){
    if(parsedArray)
    {
        tenantID = parsedArray[0];
        tenantdataset = parsedArray;
        var i = 1;
        $.each(everyField, function(key, val){
            if(key == 'tenantStatus')
            {
                $('#'+key).prop('checked', parsedArray[i] == 1 ? true : false);
            }
            else
            {
                tenantdataset[i] == null ? tenantdataset[i] = '' : null;
                $('#'+key).val(tenantdataset[i]);
                
            }
           i++; 
        });
        setFieldStatus(everyField, true);
        $('#dater, #save').prop('disabled', true);
        loadButtonStatuses(false);
    }
}

//get the next or previous button
function prevNextBtn(statusPrev)
{
    if(tenantID)
    {
       sendData(tenantID, statusPrev); 
    }
    else
    {
        var id = 'NoID';
        sendData(id, statusPrev);
    }
    
    function sendData(id, statusPrev)
    {
        $.ajax(
        {
           url: 'sendBackStuff.php?page=tenant&id='+id+'&'+'statusPN='+statusPrev,
           dataType : 'json',
           success : function(data)
           {
            if(data == '300')
            {

                $.notify("data not found","info");
                
            }
            else if(data == '400')
            {
                $.notify("error occurred","error");

            }
            else
            {
                setValueInFields(data);
            }
           }
            
        });
    }
    
}
insertFromQuery('tenant', setValueInFields);
$('#fname,#sname,#idnum, #gender, #ownerEmail,#boardingDate, #pnumber1, #kinfName,#kinSName,#kinIdNo,#kinPhoneNo,#tenantDepositAmt, #tenantMonthlyRent, #periodstart').click(function(event){
    if($('#'+event.target.id).hasClass('alert alert-danger'))
    {
        $('#'+event.target.id).removeClass('alert alert-danger');
        $('#requiredError').html('');
    }
});

});