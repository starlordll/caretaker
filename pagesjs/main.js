$(document).ready(function(){
setImageSize();
$("#submit").click(function(event) {
			//console.log("in submit");
			var usr = $("#username").val();
			var passwrd = $("#password").val();
			if (usr != "" && usr != undefined && passwrd != undefined && passwrd != "")
			{
				sendData(escape(usr), escape(passwrd));
                //$('#spinner').addClass('glyphicon glyphicon-refresh glyphicon-refresh-animate');
                $(this).html('Please wait..  ');
                $(this).append('<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>');                      
			}
			else
			{
			 $('#errmsg').html("Empty username or password !!");
             toggleError();
			}

			/* Act on the event */
});
        
        
		//console.log( (90/100)*$(window).height());
		//console.log($(document).height());
function toggleError()
{
    if($('#timeoutmessage').hasClass('alert'))
        {
            $('#timeoutmessage').removeClass('alert');
            $('#timeoutmessage').html('');
        }
}
	
function setImageSize()
{

    $(".midpanels").css("min-height", (90/100)*$(window).height());
}

function sendData(usr, passwd)
{
	$.ajax({
	   url : "validate.php",
       type : "POST",
       data : {"username" : usr, "password" : passwd},
       success : function(dt){
                if(dt == "200")
           		{
           			window.location.href = "index.php";	
           		}
           		else
           		{
           		   $('#errmsg').html(dt);
                   $("#submit").html('Sign In ');
                   $('#submit span').remove();
                    toggleError();
           		}
       },
       error : function(err){
          $('#errmsg').html(err);
          $("#submit").html('Sign In ');
          $('#submit span').remove();
          toggleError();
       }
	});
    
	

}
$(window).resize(function() {
	setImageSize();
});    
   
    
});
