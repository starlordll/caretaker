<?php
require_once('aside/sessionsmanager.php');
$page = $_SERVER['SCRIPT_FILENAME'];$regex = '/\w*\.php$/';preg_match($regex, $page, $pagearray);
$finalpg = trim(explode('.', $pagearray[0])[0]); 
$sessionHandler = new SessionManager();
$sessionHandler->runForce();  
 

  
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="bootstrap/favicon.ico">

    <title><?php echo $_SESSION['userroles'][$finalpg]['screenname'].' - Caretaker'; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    
    
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="bootstrap/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
   

    <link href="css/sticky-footer.css" rel="stylesheet">
    <script src="js/vendor/jquery.min.js"></script>
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="bootstrap/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="bootstrap/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
<link href="css/dashboard.css" rel="stylesheet">  
<link href="css/main.css" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="myjs/jquery-ui.css"/>
<style>

li a, .dropbtn {
    display: inline-block;
    /*color: white;*/
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

li a:hover, .dropdownsub:hover .dropbtn {
    /*background-color: red;*/
}

li.dropdownsub {
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    /*background-color: #f9f9f9;*/
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
}

.dropdown-content a {
    /*color: black;*/
    padding: 12px 16px;
    text-decoration: none;
    display: block;
    text-align: left;
}

/*.dropdown-content a:hover {background-color: #f1f1f1}*/

.dropdownsub:hover .dropdown-content {
    display: block;
}
</style>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">Caretaker</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="index.php">Dashboard</a></li>
            <li><a href="notifications.php">Notifications <span class="badge">4</span></a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
              <?php echo ' '.$_SESSION['user'].'  '; ?><span class="caret"></span>
            </a>
               <ul class="dropdown-menu">
                    <li><a href="validate.php?page=<?php echo $finalpg ?>">Logout</a></li>             
               </ul>
            </li>
            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            About <span class="caret"></span</a>
               <ul class="dropdown-menu">
                 <li><a href="#">System</a></li>
                 <li><a href="#">Documentation</a></li>
               </ul>
            
            </li>
               
          </ul>
          <!-- <form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Search...">
          </form> -->
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <?php 
            if(isset($_SESSION['userroles']))
            {
               $togglepgs = array('paymentperiods','users', 'company', 'notifications','notificationtemplates','settings');
               $allpagescount  = count($_SESSION['userroles']);
               $settingspgs = '';
               $i =0;
               foreach($_SESSION['userroles'] as $key => $val)
               {
                  $pageid  = $key.'Tab';
                  $pagename = $key.'.php';
                  $pagedesc = $val['screenname'];
                  $modalstr  = '';
                  if(count($val) > 1)
                  {
                      
                         $modalstr = '<li id="'.$pageid.'"><div class="dropdownsub"><a href="'.$pagename.'" class="dropbtn">'.$pagedesc.'</a><span class="sr-only">(current)</span><div class="dropdown-content">';
                           if(in_array($key, $togglepgs))
                           {
                                
                           }
                           else
                           {
                               $strsub = '';
                               foreach($val as $key2 => $val2)
                               {  
                                if($key2 != 'screenname')
                                {
                                    $strsub .= '<a href="'.$val2.'">'.$key2.'</a>';
                                }
                                
                                    
                                //"<li id='$pageid'><a href='$pagename'> $pagedesc <span class='sr-only'>(current)</span></a></li>"
                               }
                           
                           
                              
                            }
                            echo($modalstr.$strsub.'</div></div></li>');
                  }
                  else
                  {
                           $mstr = "<li id='$pageid'><a href='$pagename'> $pagedesc <span class='sr-only'>(current)</span></a></li>";
                           if(in_array($key, $togglepgs))
                           {
                              $settingspgs .= $mstr;
                    
                           }
                           else
                           {
                            echo($mstr);
                            //"<li id='$pageid'><a href='$pagename'> $pagedesc <span class='sr-only'>(current)</span></a></li>"
                           }
                          
                        
                    
                  }
                  /*
                  if($i == $allpagescount - 1 && $settingspgs != '')
                  {
                             echo("<li class='dropdown' id='headtoggle'><a class='dropdown-toggle'  data-toggle='dropdown' href=''>Configurations <span class='caret'></span></a>
                     <ul class='dropdown-menu'>".$settingspgs. "</ul></li>");
                  }
                  */
                  
                  
                  $i++;
                  
               }  
            }
            
            
            ?>
            
          </ul>
          
        </div>   