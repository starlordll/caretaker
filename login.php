<?php
session_start();
if (isset($SESSION['user'])) {
	# code...
	header("Location:index.php");
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width initial-scale=1">
<meta name="description" content="">
<script src="js/vendor/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="dist/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/sticky-footer.css">
<link rel="stylesheet" type="text/css" href="css/login.css">
<link rel="icon" href="../../favicon.ico">
<title> Login Page</title>
</head>
<body>
 <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	<div class="navbar-header">
		<a class="navbar-brand" href="#">Caretaker</a>

	</div>
	 <div id="navbar" class="navbar-collapse collapse">
     </div>     
  </nav>
<div class="container-fluid"> 
  <div class="row">
     <div class="midpanels col-sm-8 col-md-9 container-fluid" style="background: url('livingroom.jpg') no-repeat center center fixed; min-height: ;">
       <!--<img id="mainimg" src="livingroom.jpg" class="img-responsive"/>-->
     </div>

     <div class="sidebar col-xs-12 col-sm-4 col-md-3">
          <div id="errmsg"></div>
          <br/>
          <div class="form form-group">
          <div class="row">
              <div class="col-sm-3">
                  <label >Username: </label> 
              </div>   
               <div class="col-sm-9">
                  <input id="username" class="form-control" type="text" name="username" placeholder="username"/>
               </div>
          </div> 
          </div>
          
          <div class="form form-group">
          <div class="row">
                <div class="col-sm-3">
                  <label> Password: </label>
                </div>
                  <div class="col-sm-9">
                      <input id="password" class="form-control" type="password" name="password" placeholder="password"/>
                  </div>
          </div>
          </div>
                 <div class="row">
                   <div class="col-md-3">
                   </div>
                   <div class="col-md-3">
                       
                   </div>
                   <div class="col-md-6">
                        <button id="submit" class="btn btn-primary btn-block btn-md" >   Sign In</button>
                   </div>
                 </div>
                 <div class="row">
                 
                    <div class="col-md-12">
                    <br />
                    <div  id="timeoutmessage"></div></div>
                   
                 </div>
           
     </div>
    </div>
    <div class="row container-fluid footer">
       <div class="col-md-12 col-sm-12 col-xs-12">
       <p id="Copyright" class="text-muted">&copy; <?php $years = getdate(); echo $years["year"]; ?> <a href="http://www.mazecoders.com">Mazecoders.com</a></p>
    </div>
  </div>

	      
	
</div>
<script src="dist/js/bootstrap.min.js" ></script>
<script type="text/javascript" src="pagesjs/main.js"></script>
<script src="myjs/purl.js"></script>
<script src="js/vendor/holder.min.js"></script>
<script  type="text/javascript">
var paramData = purl();var getData = paramData.attr('query');if(getData != '' && getData != undefined){var matchpat = new RegExp('q=');if(matchpat.test(getData))
{var getId = getData.split('&');var getTime= getId[1];var paramValue = getId[0].split('=');if(paramValue[1] == 'timeout'){$('#timeoutmessage').addClass("alert alert-danger");
$('#timeoutmessage').html('Session Expired: More than '+ (getTime.split('=')[1])/60+ ' minutes of inactivity, please login again');}/**/}}
</script>


</body>
</html>