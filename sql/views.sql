use caretaker;
create view vwTenantPeriods
as
select a.id as tenantid, b.Id as periodid from tenant a cross join paymentperiods b;
/* -------------- */
alter view vwPaymentMain
as
select concat (b.firstName, ' ',b.secondName) as names,b.monthlyrent as Rent, case when d.cumullamt is null then 0 else d.cumullamt end as paidrent,
b.startPeriodId as OccupationPeriod, case when d.active is null then 2 when d.active = 0 then 3 else  d.active end as rentstatus,
case when e.aprtName is null then 'Unassigned' else e.aprtName end as apartmentname,c.periodName,b.id as tenantid,
c.periodDesc as Month, c.year 
from vwtenantperiods a 
left join  tenant b on a.tenantid = b.id 
left join paymentperiods c on a.periodid = c.Id 
left join cumulativepayments d on a.periodid = d.periodsid and a.tenantid = d.tenantid
left join apartment e on a.tenantid = e.tenantid
