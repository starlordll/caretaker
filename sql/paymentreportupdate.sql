use caretaker;
/*select * from tenant a 
left join vwtenantperiods b on a.id = b.tenantid
left join cumulativepayments c on a.id = c.tenantid; 
*/
select b.firstName, b.secondName,b.monthlyrent as Rent, case when d.cumullamt = null then 0 else d.cumullamt end as paidrent,
b.startPeriodId as OccupationPeriod, case when d.active = null then 'notpaid' else  d.active end as monthactive,
c.periodDesc as Month, c.year from vwtenantperiods a 
left join  tenant b on a.tenantid = b.id 
left join paymentperiods c on a.periodid = c.Id 
left join cumulativepayments d on a.periodid = d.periodsid and a.tenantid = d.tenantid 
where (c.periodName = 4 and c.year = year(now()))
order by b.id, c.periodName asc, c.year asc

