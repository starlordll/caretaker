use caretaker;
select ifnull(d.aprtName ,'Unassigned') as Apartment,ifnull(d.aprtDesc,'Unassigned') Apartment_Desc, 
case when b.monthlyrent <= 0 then d.costPerMonth else b.monthlyrent end as Amount,
c.periodDesc, c.year
from vwtenantperiods a left join tenant b on a.tenantid = b.id 
left join paymentperiods c on a.periodid = c.id left join apartment d on b.id = d.tenantid; 