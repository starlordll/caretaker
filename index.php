<?php

/*if(!isset($_SESSION['user']))
{
  header('Location:login.php');
}*/
include_once('top.php');
?>
         
        <div class="col-sm-9  col-md-10 col-xs-12" style="padding-left: 20px;padding-top: 20px; padding-right: 20px;">
          <h1 class="page-header">Dashboard</h1>

          <div class="row placeholders">
            <div class="col-xs-6 col-sm-3 placeholder" >
              <a href="tenant.php"><img src="images/tenant.gif" width="200" height="200" class="img-responsive"></a>
              <h4>Add Tenant</h4>
              <span class="text-muted">Add New Tenant</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <a href="addpayment.php"><img src="images/dollar.gif" width="200" height="200" class="img-responsive" alt="Payment placeholder thumbnail"></a>
              <h4>New Payment</h4>
              <span class="text-muted">Add a new payment to the system</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <a href="<?php 
              $dateArr = getdate();
              $urlbuild ='run.php?execute_mode=EXECUTE&project=caretaker&xmlin=overdue.xml&target_show_criteria=0&clear_session=1&rentstatus=1,2&year='.$dateArr['year'].'-'.$dateArr['mon'].'-'.$dateArr['mday'].'&periodnm='.($dateArr['mon']-1).','.$dateArr['mon']; 
              echo($urlbuild);
              
                 ?>">
              <img src="images/report.gif" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
              </a>
              <h4>Payments Due</h4>
              <span class="text-muted">View Reports</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <a id="modalstart">
              <img src="images/apartment.gif" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail"></a>
              <h4>Assign Apartment</h4>
              <span class="text-muted">Apartment Allocation</span>
              
            </div>
          </div>

          <h2 class="sub-header"> Recent Payments</h2>
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
              <thead>
                <tr>
                  <th>Transaction Id</th>
                  <th>Payment Type</th>
                  <th>Full Names</th>
                  <th>Amount</th>
                  <th>Status</th>
                  <th>Date</th>
                </tr>
              </thead>
              <tbody>
                 <?php 
                 $stmt = 'select a.transId, case when a.tranDesc = "m" then "Mpesa" when a.tranDesc ="c" then "Cheque" else "Cash" end as transDesc,concat(b.firstName, 
                 " ", b.secondName) as fullnames, a.paymentAmount,case when a.Status = "1" then "On Hold" when a.Status = "2" then "Approved" when a.Status = "3" 
                 then "Cancelled" else "Unknown" end as Status,from_unixtime(a.paymentDate, "%d/%m/%Y") as transDesc from payments a left join tenant b on a.tenantid= b.id order by a.transDtstamp desc 
                 limit 2';
                 $newdbins = new DatabaseHandler();
                 try
                 {
                 $datareceiv = $newdbins->returnSelectData($stmt);
                 foreach($datareceiv->fetchAll(PDO::FETCH_NUM) as $key)
                 {
                    echo ('<tr><td><a href="addpayment.php?id='.$key[0].'">'.$key[0].'</a></td><td>'.$key[1].'</td><td>'.$key[2].'</td><td>'.$key[3].'</td><td>'.$key[4].'</td><td>'.$key[5].'</td></tr>');
                 }
                 }
                 catch(PDOException $e)
                 {
                    //echo $e->getMessage();
                 }
                 
                 ?>
               
              </tbody>
            </table>
          </div>
        </div>
        <!-- modal code -->  
        <div class="modal fade" id="idmodal" role="dialog">
          <div class="modal-dialog">
             <div class="modal-content">
                <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal">&times;</button>
                 <h3> Assign Apartment to tenant</h3>
                </div>
                <div class="modal-body">
                   <div class="form form-group">
                   <label>Apartment to be assigned </label>
                   <select class="form-control" id="aprtctrl">
                     <option value="None">None</option>
                   </select>
                   </div>
                   
                   <div class="form form-group">
                   <label>Tenant Name</label>
                   <select class="form-control" id="tenctrl">
                     <option value="0">None</option>
                   </select>
                   </div>
                   <br />
                   <div id='errormsges'></div>
                </div>
                <div class="modal-footer">
                <button class="btn btn-info" id="aprtbtn"> Assign Apartment </button>
                </div>
             </div>
          
          
          
          </div>
           
        </div>
        <style>
          .modal-header, h3, .close {
          background-color: #5cb85c;
          color:white !important;
          text-align: center;
          font-size: 30px;
      }
         </style>
        
        <!-- end modal code -->
        
      </div>
      
    </div>
    <script type="text/javascript" src="pagesjs/assignaprt.js"></script>
<?php


include_once('bottom.php');
?>

