<?php

include_once('periodsgenerator.php');
include_once('sessionsmanager.php');
class Settings extends DatabaseHandler
{
    var $updatefields;
    var $updatedata;
    var $settingmap;
    function __construct()
    {
        parent::__construct();
        $settingmap;    
    }
    public function runSettings()
    {
        
        if(isset($_POST['action']))
        {
            switch($_POST['action'])
            {
                case 'u': //update
                    $this->runUpdate();
                    break;
                case  'g': //get settings
                     $this->returnSettings();
                     break;
                case  'pg'://generate period command
                     $this->callGeneratePeriods();
                     break;
                case  'pr'://return next generated year period
                     $this->returnNextAvailYear();
                     break;
                default:
            }
        }
        
    }
    public function callGeneratePeriods()
    {
        $stmt = 'select year from paymentperiods order by Id desc limit 1';
        $t = new PeriodGenerator();
        $currentyr = $this->connector->query($stmt)->fetchColumn();
        if($currentyr)
        {
            
            echo $t->setPeriodDefaults($currentyr+1)->createNewPeriods();
        }
        else
        {
            if(isset($_POST['year']))
            {
                if(count($_POST['year']) == 4)
                {
                   echo $t->setPeriodDefaults($_POST['year'])->createNewPeriods();
                }
                else
                {
                    echo '400';
                }
                  
            }
            
            
        }
    }
    public function returnNextAvailYear()
    {
        $stmt = 'select year from paymentperiods order by Id desc limit 1';
        $currentyr = $this->connector->query($stmt)->fetchColumn();
        echo(json_encode($currentyr ? [$currentyr+ 1 ] : ['']));
    }
    public function returnSettings()
    {
        $stmt = 'select id, field, checked from tickedsettings';
        $dt = $this->connector->query($stmt);
        echo json_encode($dt->fetchAll(PDO::FETCH_ASSOC));
    }
    public function runUpdate()
    {
        if(isset($_POST['checked']) && isset($_POST['id'])  && !empty($_POST['id']))
        {
            
               $stmt1 = 'update tickedsettings set checked = ? where id = ?';
               $updater = $this->connector->prepare($stmt1);
                 try
                 {
                   
                   $updater->execute([$_POST['checked'], $_POST['id']]);
                   echo $updater->rowCount() ? '200' : '300';
                   
                 }
                   catch(PDOException $e)
                  {
                    echo '400';//$e->getMessage();
                  }  
            
        }
     
    }
    
    
    
}

$newsettings  = new Settings();
$sessionHandler = new SessionManager();
$sessionHandler->serverPagesVerifier([$newsettings, 'runSettings']);


?>