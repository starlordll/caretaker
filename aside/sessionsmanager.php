<?php
session_start();

include_once($_SERVER['DOCUMENT_ROOT'].'/caretaker/includes/dbconnection.php');
class DatabaseHandler
{
    var $connector;
    public function __construct()
    {
       $this->connector = Dbconnector::returnconnection();
        
    }
    public function returnSelectData($query)
    {

        return $this->connector->query($query);
        
        
    }
    public function returnUpdateStatus($sqlstmt ,$data)
    {
        $preparer = $this->connector->prepare($sqlstmt);
        return $preparer->execute($data);
        
    }
    public function returnInsertStatus($sqlstmt, $data)
    {
        $preparer = $this->connector->prepare($sqlstmt);
        return $preparer->execute($data);
    }
    public function returnDelStatus($stmt)
    {
        $this->connector->execute($stmt);
    }
    
}
class RolesVerifier extends DatabaseHandler
{
    //verify if user is allowed access
    public $currentpage;
    public $pgwithext;
    public $langtypeext; 
    var $pgallowed =FALSE;
    function __construct()
    {
        parent::__construct();
        $page = $_SERVER['SCRIPT_FILENAME'];
        $regex = '/\w*\.php$/';
        preg_match($regex, $page, $matcharray);
        $this->pgwithext = trim($matcharray[0]);
        $this->currentpage =  trim(explode('.', $matcharray[0])[0]);
        $this->langtypeext = trim(explode('.', $matcharray[0])[1]);
    }
    public function checkIfAllowedPage()
    {
        //$allowedpgs = array();
        //$stmt = 'select id from screens where name= "'.$this->currentpage.'" and langtype = "'.$this->langtypeext.'"';
        //$getter  = $this->returnSelectData($stmt);
        //$screenrecord = $getter->fetch(PDO::FETCH_ASSOC);
        //$id = isset($screenrecord['id']) ? $screenrecord['id'] : '';
        if(isset($_SESSION['userroles']))
        {

    
                if(array_key_exists($this->currentpage, $_SESSION['userroles']))
                {
                    $this->pgallowed = true;
                    return $this->pgallowed;
                }
                else
                {
                    
                    foreach($_SESSION['userroles'] as $key => $val){
                    if(in_array($this->pgwithext, $val))
                    {
                        $this->pgallowed = true;
                        return $this->pgallowed;
                    }
                    
                    }
                    
                    
                    
                }
                
    

        }
        return $this->pgallowed; 
        
    }
    //public function  
}
class SetAllowedRoles extends DatabaseHandler
{
    
    function __construct()
    {
        parent::__construct();
        //new SetSystemDetails();
        $stmt = 'select b.id, b.screenDesc, b.name, ifnull(c.icondesc,"") from roles a left join screens b on a.screenid = b.id left join iconset c on b.iconid = c.id where a.userid ="'.$_SESSION['userid'].'" and b.allowed = 1 and b.parentId = 0 order by b.sortorder';
        $getdata = $this->returnSelectData($stmt);
        unset($_SESSION['userroles']);
        foreach($getdata->fetchAll(PDO::FETCH_NUM) as $key => $val)
        {
            $tquery = 'select b.name, ifnull(b.additionalinfo,b.name) as info from roles a left join screens b on a.screenid = b.id  where a.userid = '.$_SESSION['userid'].' and  b.parentId = "'.$val[0].'" and  b.allowed = 1 order by b.sortorder';
            $tdata =  $this->returnSelectData($tquery)->fetchAll(PDO::FETCH_ASSOC);
            $extrasubs = array();
            $extrasubs['screenname'] = $val[1];
            $extrasubs['iconclass'] = $val[3];
            //$extrasubs['viewpayments'] ='yipppppe';
            if(count($tdata) > 0)
            { 
                foreach($tdata as $keyy => $vall)
                {
                    $extrasubs[$vall['name']] = $vall['info'];
                    
                }
                
            }
            
            $_SESSION['userroles'][$val[2]] = $extrasubs;
            
            
        }
        //var_dump($_SESSION);
                //array_push(, $rolesarray);
     
    } 
     
}
class CheckScreens extends DatabaseHandler
{
    public static function returnScreenStatus($page)
    {
        if($page != '')
        {
           $stmt = 'select name from screens where name = "'.$page.'"';
           $getdt = Dbconnector::returnconnection()->query($stmt);
           if(count($getdt->fetch(PDO::FETCH_NUM)) > 0 )
           {
              return true;
           }
           else
           {
              return false;
           }
        }
        else
        {
            return false;
        }
        
        
        
    }
}
class SessionManager
{
    var $userset;
    var $pageallowed;
    var $curpage;
    var $timeexpired = false;
    public static $EXPIRETIME = 1500;
    public function __construct()
    {

        $this->userset = isset($_SESSION['user']) ? true : false;
        //check if user authorized to access this page
        $verifierobj = new RolesVerifier();
        $this->pageallowed = $verifierobj->checkIfAllowedPage();
        $this->curpage = $verifierobj->currentpage;
        if(isset($_SESSION['logintime']))
        {
            $this->timeexpired = time() - $_SESSION['logintime']   > self::$EXPIRETIME;
        }
        else
        {
            $this->timeexpired = true;
        }
        
        
    
        
    }
    function runForce($callbackfunction = NULL)
    {
        //verifier for pages displayed on the client side
        if($this->userset && $this->pageallowed && !$this->timeexpired)
        {
            if(!$callbackfunction)
            {
                $_SESSION['logintime'] = time();
            }
            else
            {
                //will call the main function for each page after validation has been done
                $_SESSION['logintime'] = time();
                call_user_func($callbackfunction);
            }
            
            
        }
        else if($this->userset && $this->pageallowed == false && !$this->timeexpired)
        {
            if($this->curpage == 'default')
            {
                $_SESSION['logintime'] = time();
            }
            else
            {
                $_SESSION['logintime'] = time();
                $arrayofpages  = isset($_SESSION['userroles']) ? array_keys($_SESSION['userroles']) : [];
                $fpage =  count($arrayofpages) > 0   ?  $arrayofpages[0].'.php' : 'default.php';
                header('Location:'.$fpage);
            }
            
            
        }
        else
        {
            $url = $this->timeexpired && $this->userset ?  'Location:validate.php?page=default&q=timeout&t='.self::$EXPIRETIME: 'Location:login.php';
            header($url);
        }
        
        
    }
    function serverPagesVerifier($callbackfunc)
    {
        //verifier for pages executed on the server side
        if($this->userset && !$this->timeexpired)
        {
          call_user_func($callbackfunc);
        }
        else
        {
            header('Location:login.php');
        }
    }
}


?>