<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/docs/examples/dashboard/includes/dbconnection.php');
class PeriodGenerator
{
    var $query = 'insert into paymentperiods (periodName, periodDesc, startDay, lastDay, year) values (?,?,?,?,?)';
    var $dt;
    function setPeriodDefaults($insertyr)
    {
        $febdays = date('L', strtotime("$insertyr-01-01")) ? 29 : 28;
        
        $this->dt = [[1, "January", 1, 31, $insertyr],[2, "February", 1, $febdays, $insertyr],[3, "March", 01, 31, $insertyr],
                [4, "April", 01, 30, $insertyr],[5, "May", 1, 31, $insertyr],[6, "June", 01, 30, $insertyr],
                [7, "July", 1, 31, $insertyr],[8, "August", 1, 31, $insertyr],[9, "September", 1, 30, $insertyr],[10, "October", 1, 31, $insertyr],[11, "November", 1, 30, $insertyr],[12, "December", 1, 31, $insertyr]];
        return $this;
        
    }
    function createNewPeriods()
    {
        $conn = Dbconnector::returnconnection();
        $conn->beginTransaction();
        foreach($this->dt as $row)
        {
            $preparedqry= $conn->prepare($this->query);
            try
            {
              $preparedqry->execute($row);
            }
            catch(PDOException $ex)
            {
                echo '400';//$ex->getMessage();
            }
        }
        $conn->commit();
        return $preparedqry->rowCount() ? '200' : '300';
    }
}
//$t = new PeriodGenerator();
//$t->setPeriodDefaults(2017)->createNewPeriods();


?>