<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/docs/examples/dashboard/includes/dbconnection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/docs/examples/dashboard/aside/periodsgenerator.php');
class PayDatabaseHandler
{
    var $dbconnect;
    function __construct()
    {
        $this->dbconnect = DbConnector::returnconnection();
    }
    function returnQueryDt($query)
    {
        return $this->dbconnect->prepare($query);
    }
    
    
    
}
class PaymentEngine extends PayDatabaseHandler
{
    var $periodName;
    var $year = '';
    var $tenantid = ''; //gets tenantid
    var $recordstatus;//approved will initiate addition revert will initiate deduction
    var $monthlybill;//gets monthly bill from tenant
    var $periodisactive; //checks if period in cumulative amounts is active 
    var $periodexists; 
    var $paidamount = 0;  // amount tendered from screen  
    var $periodid = '';
    var $elecamt = 0;
    var $waterbill = 0;
    var $extracosts = 0;
    var $previouspayment = 0; ///tenant had paid but not full amount
    var $prevelecbill = 0;
    var $prevwaterbill = 0;
    var $prevextracosts = 0;
    var $totalamt = 0;
    var $addingcumulative = false;
    var $cumulativeid;
    var $insertstmt;
    var $periodGeneratorObj;
    
    function __construct($status)//setPeriodDefaults
    {
        //$status is update or insert
        parent::__construct();
        $this->recordstatus = $status;
        $this->insertstmt = 'insert into cumulativepayments (periodsid, tenantid, active,  cumullamt, eleccost, watercost, extracosts) values (?,?,?,?,?,?,?)';
        $this->periodGeneratorObj = new PeriodGenerator();
        
    }
    function setValues()
    {
        if($this->recordstatus =='i')
        {
            $this->tenantid = isset($_REQUEST['tenantselect']) ? $_REQUEST['tenantselect'] : '';
            $this->paidamount = isset($_REQUEST['amount']) ? $_REQUEST['amount'] : 0;
            $this->periodid = isset($_REQUEST['paymentprds']) ? $_REQUEST['paymentprds'] : '';
            $this->elecamt = isset($_REQUEST['elecbill']) ? $_REQUEST['elecbill'] : 0;
            $this->waterbill = isset($_REQUEST['waterbill']) ? $_REQUEST['waterbill'] : 0;
            $this->extracosts = isset($_REQUEST['addcbill']) ? $_REQUEST['addcbill'] : 0;
        }
        else if($this->recordstatus == 'u')
        { 
            // set values on update
            $payid = $_REQUEST['id'];
            $getstmt = 'select paymentPeriod, tenantId, paymentAmount, elecbill,waterbill,extracosts from payments where transId = ?';
            $setVals = $this->returnQueryDt($getstmt);
            $setVals->execute([$payid]);
            $alldt = $setVals->fetch(PDO::FETCH_ASSOC);
            $this->tenantid = $alldt['tenantId'];
            $this->periodid = $alldt['paymentPeriod'];
            $this->paidamount = $alldt['paymentAmount'];
            $this->elecamt = $alldt['elecbill'];
            $this->waterbill = $alldt['waterbill'];
            $this->extracosts = $alldt['extracosts'];
            
        }
        else
        {
            
        }   
        if($this->tenantid != '')
        {
            //set rent amount
            $getmonthlybillstmt = 'select monthlyrent from tenant where id = ?';
            $dtobj = $this->returnQueryDt($getmonthlybillstmt);
            $dtobj->execute([$this->tenantid]);
            $this->monthlybill = $dtobj->fetchColumn();
            if($this->monthlybill == 0)
            {
                $getapartstmt = 'select costPerMonth from apartment where tenantid = ?';
                $tquery = $this->returnQueryDt($getapartstmt);
                $tquery->execute([$this->tenantid]);
                $rent = $tquery->fetchColumn();
                $this->monthlybill =  $rent ?  $rent : 0;
                
                
            }
        }
        return $this;
    }
    function periodExistsActive()
    {
                    $this->totalamt = $this->previouspayment + $this->paidamount;
                    $updatestmt  = 'update cumulativepayments set active = ?, cumullamt = ?, eleccost = ? , watercost = ? , extracosts = ? where periodsid = ? and tenantid = ?';
                    if($this->totalamt < $this->monthlybill)
                    {
                        
                        $dtthings = $this->returnQueryDt($updatestmt)->execute([1, $this->totalamt, $this->prevelecbill + $this->elecamt, $this->prevwaterbill + $this->waterbill, $this->prevextracosts + $this->extracosts,$this->periodid, $this->tenantid]);
                        $this->addingcumulative = $dtthings ? true : false;
                    }
                    else if ($this->totalamt == $this->monthlybill)
                    {
                        
                        $affectedrows = $this->returnQueryDt($updatestmt)->execute([0, $this->totalamt, $this->prevelecbill + $this->elecamt, $this->prevwaterbill + $this->waterbill, $this->prevextracosts + $this->extracosts,$this->periodid, $this->tenantid]);
                        $this->addingcumulative = $affectedrows ? true : false;
                    }
                    else
                    {
                        $numofmonths = $this->monthlybill > 0 ? round((float)(($this->totalamt)/$this->monthlybill), 2) : 0;
                        $counter = (int)$numofmonths;
                        $activeextraamt = $this->totalamt - ($counter * $this->monthlybill);
                        $i =  0;
                        while($counter)
                        {
                            if($i < 1)
                            {
                                
                                $affectedrows = $this->returnQueryDt($updatestmt)->execute([0, $this->monthlybill, $this->prevelecbill + $this->elecamt, $this->prevwaterbill + $this->waterbill, $this->prevextracosts + $this->extracosts,$this->periodid, $this->tenantid]);
                                $this->addingcumulative = $affectedrows ? true : false;
                                $this->periodName += 1;
                                $this->periodid = $this->getCurrentPeriodId();
                            }
                            else
                            {
                                if($this->periodName <= 12)
                                {
                                   
                                   $querier = $this->returnQueryDt($this->insertstmt)->execute([$this->periodid, $this->tenantid,0, $this->monthlybill, 0.00,0.00, 0.00]);
                                   $this->addingcumulative = $querier ? true : false;
                                   //will be inserting monthly bill consequetively
                                   $this->periodName += 1;
                                   $this->periodid =  $this->getCurrentPeriodId();
                                }
                                else
                                {
                                    
                                   $this->newYearSetter();
                                   $querier = $this->returnQueryDt($this->insertstmt)->execute([$this->periodid, $this->tenantid,0, $this->monthlybill, 0.00,0.00, 0.00]);
                                   $this->addingcumulative = $querier ? true : false;
                                   $this->periodName += 1;
                                   $this->periodid =  $this->getCurrentPeriodId();
                                }
                            }
                            
                            
                            $counter --;
                            $i++;
                            
                            
                            
                        }
                        if($activeextraamt > 0)
                        {
                            if($this->periodName <= 12)
                            {
                                
                              $querier = $this->returnQueryDt($this->insertstmt)->execute([$this->periodid, $this->tenantid,1, $activeextraamt, 0.00,0.00, 0.00]);
                               $this->addingcumulative = $querier ? true : false;
                            }
                            else
                            {
                                $this->newYearSetter();
                                $querier = $this->returnQueryDt($this->insertstmt)->execute([$this->periodid, $this->tenantid,1, $activeextraamt, 0.00,0.00, 0.00]);
                                $this->addingcumulative = $querier ? true : false;
                                $this->periodName += 1;
                                $this->periodid =  $this->getCurrentPeriodId();
                            }
                        }
                        
                    }

    }
    function existsPeriodNotactive()
    {
                    //period not active 
                    
                     if($this->paidamount < $this->monthlybill)
                     {
                        $dtthings = $this->returnQueryDt($this->insertstmt)->execute( [$this->periodid, $this->tenantid,1, $this->paidamount, $this->elecamt, $this->waterbill, 
                        $this->extracosts]);
                        $this->addingcumulative = $dtthings ? true : false;
                    }
                    else if ($this->paidamount == $this->monthlybill)
                    {

                        $affectedrows = $this->returnQueryDt($this->insertstmt)->execute([$this->periodid, $this->tenantid,0, $this->paidamount, $this->elecamt, $this->waterbill, 
                        $this->extracosts]);
                        $this->addingcumulative = $affectedrows ? true : false;
                    }
                    else
                    {
                        $numofmonths = $this->monthlybill > 0 ? round((float)(($this->paidamount)/$this->monthlybill), 2) : 0;
                        $counter = (int)$numofmonths;
                        $extraamt = $this->paidamount - ($counter * $this->monthlybill);
                        $i = 0 ;//supposed to insert the water, elec and extracosts bill in first record
                        while($counter)
                        {   
                          if($i < 1)
                            {
                                
                                
                                $dtinserted = $this->returnQueryDt($this->insertstmt)->execute([$this->periodid, $this->tenantid,0, 
                                $this->monthlybill, $this->elecamt, $this->waterbill, $this->extracosts]);
                                $this->addingcumulative = $dtinserted ? true :  false;
                                $this->periodName += 1;
                                $this->periodid = $this->getCurrentPeriodId();
                           }
                           else
                           {
                             
                               if($this->periodName <= 12)
                              {
                             
                                //make sure current period doesnt exist, if exists jump to next period
                                $dtinserted = $this->returnQueryDt($this->insertstmt)->execute([$this->periodid, $this->tenantid,0, $this->monthlybill, 0.00,0.00, 0.00]);
                                $this->addingcumulative = $dtinserted ? true :  false;
                                $this->periodName += 1;
                                $this->periodid = $this->getCurrentPeriodId();
                                
                              }
                        
                             else
                              {
                              
                                $this->newYearSetter();
                                $dtinserted = $this->returnQueryDt($this->insertstmt)->execute([$this->periodid, $this->tenantid,0, $this->monthlybill, 0.00,0.00, 0.00]);
                                $this->addingcumulative = $dtinserted ? true : false;
                                $this->periodName += 1;
                                $this->periodid =  $this->getCurrentPeriodId();
                              }
                            
                            }
                            $counter--;
                            $i++;                       
                            
                        }
                        if($extraamt > 0)
                        {
                            if($this->periodName <= 12)
                            {
                                
                              $querier = $this->returnQueryDt($this->insertstmt)->execute([$this->periodid, $this->tenantid,1, $extraamt, 0.00,0.00, 0.00]);
                               $this->addingcumulative = $querier ? true : false;
                            }
                            else
                            {
                                
                               $this->newYearSetter();
                               $querier = $this->returnQueryDt($this->insertstmt)->execute([$this->periodid, $this->tenantid,1, $extraamt, 0.00,0.00, 0.00]);
                               $this->addingcumulative = $querier ? true : false;
                               $this->periodName += 1;
                               $this->periodid =  $this->getCurrentPeriodId();
                            }
                        }
                        
                    }
    }
    function getCurrentPeriodId()
    {
        //after incrementing period and year or both check if period exists in paymentperiods
        $anotherqry = 'select id from paymentperiods where year = ? and periodName = ?';
        $dopereturn = $this->returnQueryDt($anotherqry);
        $dopereturn->execute([$this->year, $this->periodName]);
        return $dopereturn->fetchColumn();
    }
    function runPaymentProcess()
    {
        if($this->periodid != '' && $this->tenantid != '' && $this->paidamount>= 0)
        {
            $setyrstmt = 'select periodName, year from paymentperiods where id = ?';
            $firstqry = $this->returnQueryDt($setyrstmt);
            $firstqry->execute([$this->periodid]);
            $selectdt = $firstqry->fetch(PDO::FETCH_ASSOC);
            $this->year = $selectdt['year'];
            $this->periodName = $selectdt['periodName'];
            $stmt = 'select b.id, b.active, b.cumullamt, b.eleccost, b.watercost, b.extracosts from cumulativepayments b where b.periodsId = ? and b.tenantid = ?';
            $dtgetter = $this->returnQueryDt($stmt);
            $dtgetter->execute([$this->periodid, $this->tenantid]);
            $periodDt = $dtgetter->fetch(PDO::FETCH_ASSOC);
            
            $this->periodisactive = isset($periodDt['active']) ? $periodDt['active'] : false;
            $this->cumulativeid = isset($periodDt['id']) ? $periodDt['id'] : ''; 
            if($this->cumulativeid)
            {
                
                if($this->periodisactive)
                {
                    $this->periodexists = true;
                    $this->periodisactive = $periodDt['active'];
                    $this->cumulativeid = $periodDt['id'];
                    $this->previouspayment = $periodDt['cumullamt'];
                    $this->prevelecbill = $periodDt['eleccost'];
                    $this->prevwaterbill =$periodDt['watercost'];
                    $this->prevextracosts = $periodDt['extracosts'];
                    $this->periodExistsActive();
                }
                else
                {
                    $this->periodisactive = false;
                    if($this->periodName <= 12)
                    {
                        $this->periodName += 1;
                        $this->periodid = $this->getCurrentPeriodId();
                        $this->existsPeriodNotactive();
                    }
                    else
                    {
                        $this->newYearSetter();
                    }
                 }         
            
            }
            else
            {
                //no period exists->
                $this->periodexists = false; //add new period
                $this->periodisactive = false;
                if($this->periodName <= 12)
                {
                     $this->existsPeriodNotactive();
                }
                else
                {
                        $this->newYearSetter();
                         //create new periods
                        
                }
                
                //get last active period
                //select last period check if active if active start above process
                //if not active start above process  
                //because the period not active and period not doestnt exist are similar
                // --  we can ruse exists period not active  
            }      





        }
        return $this->addingcumulative;
      }
      function newYearSetter()
      {
        //sets new year and period if months are greater than 12
                        $this->year += 1;
                        $this->periodName = 1;
                        $periodchecker = $this->getCurrentPeriodId();
                        if($periodchecker)
                        {
                            
                            $this->periodid = $periodchecker;
                        }
                        else
                        {
                            $this->periodGeneratorObj->setPeriodDefaults($this->year)->createNewPeriods();
                            $this->periodid = $this->getCurrentPeriodId();
                            
                        }
      }
    
}






?>