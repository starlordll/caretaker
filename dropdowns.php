<?php

session_start();
if(isset($_SESSION['user']))
{

   include_once('includes/dbconnection.php');
   $conn = DbConnector::returnconnection();
   
    class Databaseops
    {
        var $stmt;
        var $params;
        function buildQuery()
        {
            
            global $conn;
            $queryObj = $conn->prepare($this->stmt);
            $queryObj->execute($this->params ? $this->params : []);  
            return $queryObj;
            
        }
        function returnData($df){
            
           $dt = $df->fetchAll(PDO::FETCH_NUM);
           if($dt)
           {
            return $dt;
           } 
           else
           {
            return [];
           } 
           
        }
        
        
    }
    class Accounts extends Databaseops
    {
        
        function __construct()
        {
            $this->stmt = 'select accId, accName from accounts where active = ?';
            $this->params = [1];
        }
        function returnAccounts()
        {
           
           
            echo(json_encode($this->returnData($this->buildQuery())));
            
        } 
        
    }
    class Tenant extends Databaseops
    {
        function __construct()
        {
            //sets table name
           $this->stmt = 'select id, firstName, secondName from tenant where isActive = ?';
           $this->params = [1];
        }
        function returnTenant()
        {
            echo(json_encode($this->returnData($this->buildQuery())));
            
        }   
    }
    class Blocks extends Databaseops
    {
        
        function __construct()
        {
            $this->stmt = 'select blockId, blockName from blocks';
            $this->params = [];
        
        }
        function returnBlocks()
        {
            echo(json_encode($this->returnData($this->buildQuery())));
            
        }   
    }
    class Roles extends Databaseops
    {
        function __construct()
        {
            $this->stmt = 'select id, name from roles';
            $this->params = [];
            
        }
        function returnRoles()
        {
            echo(json_encode($this->returnData($this->buildQuery())));
            
        } 
    }
    class Users extends Databaseops
    {
        
        function __construct()
        {
            
            $this->stmt = 'select userid, username from users';
            $this->params = [];
            
        }
        function returnUsers()
        {
            echo(json_encode($this->returnData($this->buildQuery())));
            
        } 
    }
    class Estates extends Databaseops
    {
        function __construct()
        {
            $this->stmt = 'select estateId, estateName from estates';
            $this->params = [];
            
        }
        function returnEstates()
        {
            echo(json_encode($this->returnData($this->buildQuery())));
            
        } 
    }
    class Apartments extends Databaseops
    {
        function __construct()
        {
            $this->stmt = 'select a.aprtName, a.aprtDesc, concat(ifnull(b.firstName, "Unassigned"), " ", ifnull(b.secondName, "")) as fullnames from apartment a left join tenant b on a.tenantid = b.id';
            $this->params = [];
            
        }
        function returnApartments()
        {
            echo(json_encode($this->returnData($this->buildQuery())));
            
        } 
    }
    class Periods extends Databaseops
    {
        function __construct()
        {
            $this->stmt = 'select Id, periodName, periodDesc, year from paymentperiods';
            $this->params = [];
        }
        function returnPeriods()
        {
            echo(json_encode($this->returnData($this->buildQuery())));
        }
    }
    class PaymentTenantPeriods extends Databaseops
    {
        var $tenantid;
        var $rent;
        var $phonenum = 0;
        var $startperiod;
        function __construct()
        {
            //send periods based on tenant
            $this->tenantid = isset($_REQUEST['tenantid']) ? $_REQUEST['tenantid'] :  '';
            $this->rent = '0';                       
        }
        function checkLastPeriodToSend()
        {
            if($this->tenantid != 'None')
            {
            $this->stmt = 'select b.periodsid, b.active,b.cumullamt, a.monthlyrent,a.paymentPhoneNo1, a.startPeriodId  from tenant a left join cumulativepayments b on a.id = b.tenantid where a.id = ? 
order by b.periodsid desc limit 1';
            $this->params = [$this->tenantid];
            $arrdt =$this->returnData($this->buildQuery());
            $this->rent = $arrdt[0][3];
            $this->phonenum = $arrdt[0][4];
            $this->startperiod = $arrdt[0][5];
               if($this->rent <= 0)
              {
                // assign rent from apartments
                $this->stmt = 'select costPerMonth from apartment where tenantid = ?';
                $this->params = [$this->tenantid];
                $val = $this->buildQuery()->fetchColumn();
                $this->rent =  $val ? $val : 0;
                
                
              }
              if($arrdt[0][0])
              {
                if($arrdt[0][1])
                {
                    $this->stmt = 'select Id, periodName, periodDesc, year from paymentperiods where Id >= ? order by id asc';
                    $this->params = [$arrdt[0][0]];
                    $existingpay = $arrdt[0][2];
                    $dtarray = $this->returnData($this->buildQuery());
                    array_push($dtarray, [$this->rent - $existingpay, $this->phonenum]);
                    echo(json_encode($dtarray));
                }
                else
                {  
                    $this->stmt = 'select Id, periodName, periodDesc, year from paymentperiods where Id > ? order by id asc';
                    $this->params = [$arrdt[0][0]];
                    $dtarray = $this->returnData($this->buildQuery());
                    array_push($dtarray, [$this->rent, $this->phonenum]);
                    echo(json_encode($dtarray));
                    
                }
                
              }
            else
              {
                $this->stmt = 'select Id, periodName, periodDesc, year from paymentperiods where Id >= ? order by id asc';
                $this->params = [$this->startperiod];
                $dtarray = $this->returnData($this->buildQuery());
                array_push($dtarray, [$this->rent,$this->phonenum]);
                echo(json_encode($dtarray));
                
                //$this->returnPeriodNone($this->rent, $this->phonenum);
                            
              }
            }
            else
            {

                $this->returnPeriodNone('0.00', $this->phonenum);
            }
            
            
        }
        function returnPeriodNone($stmt, $tenantrent, $phonenum)
        {
                $this->stmt = 'select Id, periodName, periodDesc, year from paymentperiods where year >= ? order by id asc';
                $date = getdate();
                $this->params = [$date['year']];
                $dtarray = $this->returnData($this->buildQuery());
                
                array_push($dtarray, [$tenantrent,$phonenum]);
                echo(json_encode($dtarray));
        }
        
    }

    if(!empty($_REQUEST["page"]) && isset($_REQUEST["page"]))
    {
        if(isset($_REQUEST['dropdownid']) && !empty($_REQUEST['dropdownid']))
        {
            $pagesarray = array('apartments', 'profile', 'blocks', 'payment');
            if(in_array($_REQUEST["page"],$pagesarray))
            {
                switch($_REQUEST["dropdownid"])
                {
                    
                    case 'accounts':
                         $accounts = new Accounts();
                         $accounts->returnAccounts();
                    break;
                    case 'tenant':
                         $tenant = new Tenant();
                         $tenant->returnTenant();
                    break;
                    case 'blocks':
                         $block = new Blocks();
                         $block->returnBlocks();
                    break;
                    case 'roleid':
                         $role = new Roles();
                         $role->returnRoles();
                    break;
                    case 'userid':
                         $user = new Users();
                         $user->returnUsers();
                    break;
                    case 'estateId':
                         $estate = new Estates();
                         $estate->returnEstates();
                    break;
                    case 'periods':
                         $period = new Periods();
                         $period ->returnPeriods();
                    break;
                    case 'paymenttenant':
                         $tenantp = new PaymentTenantPeriods();
                         $tenantp->checkLastPeriodToSend();
                    break;
                    case 'apartment':
                         $aprt = new Apartments();
                         $aprt->returnApartments();
                    break;
                    default:
                    
                    
                    
                    
                    
                    
                    
                }
                
                
                
                
                
                
            }
            
            
            
            
        }
    }
     
}


?>